#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   analyses.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   19 Sep 2022

@brief  handling of analyses (e.g., rankings)

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import (
    Blueprint,
    redirect,
    render_template,
    url_for,
)
from werkzeug.exceptions import abort
from flask_wtf import FlaskForm
from wtforms import (
    SubmitField,
    SelectField,
)
from flask_login import current_user


from ..auth import login_required
from ..db import get_db


bp = Blueprint("analysis", __name__)


@bp.route("/analysis/create/", methods=("GET", "POST"))
@login_required
def create():
    db, orms = get_db()
    with db.session.begin():
        if len(current_user.challenges_owned) == 0:
            abort(401, "You don't own any challenges to rank")
        options = [orms.SideSideChallengeAnalysis.name]

        challenge_options = [
            (str(pk), title)
            for (pk, title) in db.session.query(orms.Challenge.pk, orms.Challenge.name)
        ]

        class NewRankingForm(FlaskForm):
            ranking_type = SelectField("Type of Ranking", choices=options)
            challenges = SelectField("Challenge", choices=challenge_options)
            submit = SubmitField("Add")
            cancel = SubmitField("Cancel")

        form = NewRankingForm()
        if form.validate_on_submit():
            if form.cancel.data:
                return redirect(url_for("analysis.create"))
            analysis_type = form.ranking_type.data
            challenge = db.session.get(orms.Challenge, int(form.challenges.data))
            if challenge is None:
                abort(501, "Unknown challenge")
            if analysis_type == orms.SideSideChallengeAnalysis.name:
                ranking = orms.SideSideChallengeAnalysis(challenges=[challenge])
            else:
                abort(501, "Unknown ranking type")
            db.session.add(ranking)
            db.session.flush()

            return redirect(
                url_for(
                    "analysis.view",
                    **dict(challenge_pk=challenge.pk, analysis_pk=str(ranking.pk)),
                )
            )
        return render_template("analysis/create.html", form=form)


@bp.route("/analysis/public_view/<int:analysis_pk>/<int:challenge_pk>")
def public_view(analysis_pk, challenge_pk):
    db, orms = get_db()
    with db.session.begin():
        analysis = db.session.get(orms.AnalysisBase, analysis_pk)
        if analysis is None:
            abort(404, "Analysis not found")
        challenge = db.session.get(orms.Challenge, challenge_pk)
        if challenge is None:
            abort(404, "Challenge not found")
        return render_template(
            "analysis/public_view.html", analysis=analysis, challenge=challenge
        )


@bp.route("/analysis/view/<int:analysis_pk>/<int:challenge_pk>")
@login_required
def view(analysis_pk, challenge_pk):
    db, orms = get_db()
    with db.session.begin():
        challenge = db.session.get(orms.Challenge, challenge_pk)
        if challenge is None:
            abort(404, "Challenge not found")
        if (
            not current_user.is_admin
            and current_user not in challenge.owners
            and current_user not in challenge.djudges
        ):
            abort(
                403,
                (
                    f"Only adminss, djudges, and owners of {challenge.name} are "
                    "allowed to see this"
                ),
            )
        analysis = db.session.get(orms.AnalysisBase, analysis_pk)
        if analysis is None:
            abort(404, "Analysis not found")
        return render_template(
            "analysis/view.html", analysis=analysis, challenge=challenge
        )
