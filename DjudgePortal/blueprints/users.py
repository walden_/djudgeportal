#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   users.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   16 Aug 2022

@brief  handling of users

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from flask import (
    Blueprint,
    flash,
    redirect,
    render_template,
    url_for,
)
from werkzeug.exceptions import abort

from ..db import get_db
from .. import orms


bp = Blueprint("user", __name__)


@bp.route("/user/index")
def index():
    db_session = get_db()
    with db_session.begin():
        users = db_session.query(orms.User).filter_by(is_admin=False).all()

        return render_template("user/index.html", users=users)


@bp.route("/user/<string:username>/")
def view_by_name(username):
    db_session = get_db()
    with db_session.begin():
        user = (
            db_session.query(orms.User)
            .filter_by(user_name_lower=username.lower())
            .one_or_none()
        )
        if user is None:
            abort(404, f"User '{username}' is not known")

        groups = db_session.query(orms.ChallengeGroup).all()
        return render_template("user/view.html", user=user, groups=groups)


@bp.route("/user/<int:pk>")
def view(pk):
    db_session = get_db()
    with db_session.begin():
        user = db_session.get(orms.User, pk)
        if not user:
            flash("User does not exist", "error")
        return redirect(url_for("user.view_by_name", username=user.user_name))


# @bp.route('/user/<int:user_pk>/<int:challenge_pk>')
# def view_per_challenge(user_pk, challenge_pk):
#     db_session = get_db()
#     with db_session.begin():
#         user = db_session.get(orms.User, user_pk)
#         if not user:
#             flash("User does not exist", "error")
#         challenge = db_session.get(orms.Challenge, challenge_pk)
#         if not challenge:
#             flash("Challenge does not exist", "error")
#         return render_template('user/view_per_challenge.html', user=user)
#
