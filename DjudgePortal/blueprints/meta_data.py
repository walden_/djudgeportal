#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   meta_data.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   06 Jun 2023

@brief  handling of metadata validation

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import Blueprint, redirect, url_for, render_template, request

from werkzeug.exceptions import abort
from flask_wtf import FlaskForm
from wtforms import (
    SubmitField,
    Form,
    FieldList,
    FormField,
    StringField,
    BooleanField,
    SelectMultipleField,
    widgets,
)

from collections import namedtuple
from flask_login import current_user

from ..auth import login_required
from ..db import get_db
from .. import orms


bp = Blueprint("meta_data", __name__)


class HashtagForm(Form):
    tag = StringField("#Tag")
    invalid = BooleanField("Invalid", render_kw={"class": "meta_data_form"})
    delete = SubmitField("Delete Tag")


class HardwareForm(Form):
    descrip = StringField("Name", render_kw={"class": "meta_data_form"})
    invalid = BooleanField("Invalid", render_kw={"class": "meta_data_form"})
    delete = SubmitField("Delete Item", render_kw={"class": "meta_data_form"})
    tags = FieldList(FormField(HashtagForm, label=""), label="#Tags")
    new_tag = SelectMultipleField(choices=[], coerce=int, validate_choice=False)
    add_tag = SubmitField("Add `Tag")


class HardwareList(Form):
    items = FieldList(FormField(HardwareForm, label=""))
    descrip = StringField("Name")
    add = SubmitField("Add Item")


class SoftwareForm(Form):
    brand = StringField("Brand", render_kw={"readonly": True})
    product = StringField("Product")
    invalid = BooleanField("Invalid")
    delete = SubmitField("Delete Item")


class SoftwareList(Form):
    items = FieldList(FormField(SoftwareForm), label="")
    brand = StringField("Brand")
    product = StringField("Product")
    add = SubmitField("Add Item")


class SelectMultipleFieldWithCheckboxes(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()


class ValidationForm(FlaskForm):
    razors = FormField(HardwareList)
    brushes = FormField(HardwareList)
    lathers = FormField(SoftwareList)
    postshaves = FormField(SoftwareList)
    fragrances = FormField(SoftwareList)

    sponsor_points = FieldList(
        SelectMultipleFieldWithCheckboxes(choices=[], coerce=str, validate_choice=False)
    )

    submit = SubmitField("Validate meta data")
    cancel = SubmitField("Clear form and start over")


@bp.route("/meta_data/validate/<int:sotd_association_pk>", methods=("GET", "POST"))
@login_required
def validate(sotd_association_pk):
    db_session = get_db()
    with db_session.begin():
        sotd_association = db_session.get(orms.SOTDAssociation, sotd_association_pk)
        if sotd_association is None:
            abort(404, "this sotd_association does not exist")
        if not current_user.pk == sotd_association.djudge_pk:
            abort(403, "You are not authorised to see this.")

        sotd = sotd_association.sotd
        challenge = sotd_association.challenge
        meta_data = sotd.meta_for_challenge(challenge)

        razor_data = list()
        brush_data = list()
        lather_data = list()
        postshave_data = list()
        fragrance_data = list()

        softwaretuple = namedtuple("software", "brand product invalid")
        for m in meta_data:
            if isinstance(m.item, orms.WSDBRazor):
                razor_data.append(
                    (
                        m,
                        dict(
                            descrip=m.item.name,
                            invalid=m.djudicial_dq,
                            tags=[
                                {
                                    "tag": assoc.hashtag.tag,
                                    "invalid": assoc.djudicial_dq,
                                }
                                for assoc in m.hashtag_associations
                            ],
                        ),
                    )
                )
            elif isinstance(m.item, orms.WSDBBrush):
                brush_data.append(
                    (
                        m,
                        dict(
                            descrip=m.item.name,
                            invalid=m.djudicial_dq,
                            tags=[
                                {
                                    "tag": assoc.hashtag.tag,
                                    "invalid": assoc.djudicial_dq,
                                }
                                for assoc in m.hashtag_associations
                            ],
                        ),
                    )
                )
            elif isinstance(m.item, orms.WSDBLather):
                lather_data.append(
                    (m, softwaretuple(m.item.brand, m.item.product, m.djudicial_dq))
                )
            elif isinstance(m.item, orms.WSDBPostshave):
                postshave_data.append(
                    (m, softwaretuple(m.item.brand, m.item.product, m.djudicial_dq))
                )
            elif isinstance(m.item, orms.WSDBFragrance):
                fragrance_data.append(
                    (m, softwaretuple(m.item.brand, m.item.product, m.djudicial_dq))
                )
        form = HardwareForm(data=razor_data)

        data = {
            "razors": {"items": [i[1] for i in razor_data]},
            "brushes": {"items": [i[1] for i in brush_data]},
            "lathers": {"items": [i[1] for i in lather_data]},
            "postshaves": {"items": [i[1] for i in postshave_data]},
            "fragrances": {"items": [i[1] for i in fragrance_data]},
        }

        form = ValidationForm(data=data)

        if request.method == "GET":
            while len(form.sponsor_points) < len(challenge.sponsor_points):
                form.sponsor_points.append_entry()
            for i, sponsors in enumerate(challenge.sponsor_points):
                form.sponsor_points[i].label.text = sponsors.title
                form.sponsor_points[i].choices = [
                    (f"{sp.pk}-{sponsors.pk}", sp.name) for sp in sponsors.sponsors
                ]
                uses = (
                    db_session.query(orms.SponsorUse)
                    .filter_by(sotd_association=sotd_association)
                    .filter_by(sponsor_points=sponsors)
                    .all()
                )
                data = [f"{use.sponsor_pk}-{use.sponsor_points_pk}" for use in uses]
                form.sponsor_points[i].data = data

        for razor_info, form_razor in zip(razor_data, form.razors.items):
            already_taken = set(razor_info[0].hashtags)
            choices = list()
            for scavenger_hunt in challenge.razor_scavenger_hunts:
                choices += [
                    (tag.pk, tag.tag)
                    for tag in scavenger_hunt.tags
                    if tag not in already_taken
                ]
            form_razor.new_tag.choices = choices

        for brush_info, form_brush in zip(brush_data, form.brushes.items):
            already_taken = set(brush_info[0].hashtags)
            choices = list()
            for scavenger_hunt in challenge.brush_scavenger_hunts:
                choices += [
                    (tag.pk, tag.tag)
                    for tag in scavenger_hunt.tags
                    if tag not in already_taken
                ]
            form_brush.new_tag.choices = choices

        if form.validate_on_submit():
            if form.cancel.data:
                return redirect(
                    url_for(
                        "meta_data.validate", sotd_association_pk=sotd_association_pk
                    )
                )

            form_point_dict = {sp_p.pk: set() for sp_p in challenge.sponsor_points}
            for form_points in form.sponsor_points:
                sponsor_points_pks = {int(d.split("-")[1]) for d in form_points.data}
                if len(sponsor_points_pks) != 1:
                    raise Exception(
                        "I shouldn't get mixed sponsor_points data, but got "
                        f"{form_points}"
                    )
                sponsor_points_pk = sponsor_points_pks.pop()
                needed = {int(val.split("-")[0]) for val in form_points.data}
                form_point_dict[sponsor_points_pk] |= needed
            for sponsor_points_pk, needed in form_point_dict.items():
                uses = (
                    db_session.query(orms.SponsorUse)
                    .filter_by(sotd_association=sotd_association)
                    .filter_by(sponsor_points_pk=sponsor_points_pk)
                    .all()
                )
                present = {u.sponsor_pk for u in uses}
                sponsor_pks_to_add = needed - present
                sponsor_pks_to_delete = present - needed
                db_session.add_all(
                    (
                        orms.SponsorUse(
                            sponsor_points_pk=sponsor_points_pk,
                            sotd_association=sotd_association,
                            sponsor_pk=pk,
                        )
                        for pk in sponsor_pks_to_add
                    )
                )
                for sponsor_pk in sponsor_pks_to_delete:
                    db_session.delete(sotd_association.sponsor_uses[sponsor_pk])

            for item, form_razor in zip((i[0] for i in razor_data), form.razors.items):
                item.djudicial_dq = form_razor.invalid.data
                if form_razor.delete.data:
                    db_session.delete(item)
                for item_hashtag_assoc, form_hashtag in zip(
                    item.hashtag_associations, form_razor.tags
                ):
                    assert item_hashtag_assoc.hashtag.tag == form_hashtag.tag.data
                    item_hashtag_assoc.djudicial_dq = form_hashtag.invalid.data
                    if form_hashtag.delete.data:
                        db_session.delete(item_hashtag_assoc)
                if form_razor.add_tag.data:
                    tag_pks = form_razor.new_tag.data
                    hashtags = (
                        db_session.get(orms.WSDBHashTag, tag_pk) for tag_pk in tag_pks
                    )
                    item.hashtags += hashtags

            if form.razors.add.data:
                name = form.razors.descrip.data
                form_razor = (
                    db_session.query(orms.WSDBRazor).filter_by(name=name).one_or_none()
                )
                if form_razor is None:
                    form_razor = orms.WSDBRazor(name=name)
                association = orms.SOTDMetaDataAssociation(
                    sotd_association=sotd_association, item=form_razor
                )
                db_session.add_all((form_razor, association))

            for item, form_brush in zip((i[0] for i in brush_data), form.brushes.items):
                if form_brush.delete.data:
                    db_session.delete(item)
                item.djudicial_dq = form_brush.invalid.data
                for item_hashtag_assoc, form_hashtag in zip(
                    item.hashtag_associations, form_brush.tags
                ):
                    assert item_hashtag_assoc.hashtag.tag == form_hashtag.tag.data
                    item_hashtag_assoc.djudicial_dq = form_hashtag.invalid.data
                    if form_hashtag.delete.data:
                        db_session.delete(item_hashtag_assoc)
                if form_brush.add_tag.data:
                    tag_pks = form_brush.new_tag.data
                    hashtags = (
                        db_session.get(orms.WSDBHashTag, tag_pk) for tag_pk in tag_pks
                    )
                    item.hashtags += hashtags

            if form.brushes.add.data:
                name = form.brushes.descrip.data
                form_brush = (
                    db_session.query(orms.WSDBBrush).filter_by(name=name).one_or_none()
                )
                if form_brush is None:
                    form_brush = orms.WSDBBrush(name=name)
                association = orms.SOTDMetaDataAssociation(
                    sotd_association=sotd_association, item=form_brush
                )
                db_session.add_all((form_brush, association))

            for item, lather in zip((i[0] for i in lather_data), form.lathers.items):
                if lather.delete.data:
                    db_session.delete(item)
                item.djudicial_dq = lather.invalid.data

            if form.lathers.add.data:
                brand = form.lathers.brand.data
                product = form.lathers.product.data
                lather = (
                    db_session.query(orms.WSDBLather)
                    .filter_by(brand=brand, product=product)
                    .one_or_none()
                )
                if lather is None:
                    lather = orms.WSDBLather(brand=brand, product=product)
                association = orms.SOTDMetaDataAssociation(
                    sotd_association=sotd_association, item=lather
                )
                db_session.add_all((lather, association))

            for item, postshave in zip(
                (i[0] for i in postshave_data), form.postshaves.items
            ):
                if postshave.delete.data:
                    db_session.delete(item)
                item.djudicial_dq = postshave.invalid.data

            if form.postshaves.add.data:
                brand = form.postshaves.brand.data
                product = form.postshaves.product.data
                postshave = (
                    db_session.query(orms.WSDBPostshave)
                    .filter_by(brand=brand, product=product)
                    .one_or_none()
                )
                if postshave is None:
                    postshave = orms.WSDBPostshave(brand=brand, product=product)
                association = orms.SOTDMetaDataAssociation(
                    sotd_association=sotd_association, item=postshave
                )
                db_session.add_all((postshave, association))

            for item, fragrance in zip(
                (i[0] for i in fragrance_data), form.fragrances.items
            ):
                if fragrance.delete.data:
                    db_session.delete(item)
                item.djudicial_dq = fragrance.invalid.data

            if form.fragrances.add.data:
                brand = form.fragrances.brand.data
                product = form.fragrances.product.data
                fragrance = (
                    db_session.query(orms.WSDBFragrance)
                    .filter_by(brand=brand, product=product)
                    .one_or_none()
                )
                if fragrance is None:
                    fragrance = orms.WSDBFragrance(brand=brand, product=product)
                association = orms.SOTDMetaDataAssociation(
                    sotd_association=sotd_association, item=fragrance
                )
                db_session.add_all((fragrance, association))
            db_session.flush()
            challenge.update_tally_for_user(sotd.author, db_session)
            sotd_association.meta_data_checked = True

            if form.submit.data:
                url = url_for(
                    "djudgement.adjudication_redirection",
                    sotd_association_pk=sotd_association_pk,
                )

            else:
                url = url_for(
                    "meta_data.validate", sotd_association_pk=sotd_association_pk
                )
            return redirect(url)

        return render_template(
            "meta_data/validate.html",
            form=form,
            sotd_association=sotd_association,
            current_user=current_user,
            categories=[],
        )
