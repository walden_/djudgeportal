#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   sotds.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   16 Aug 2022

@brief  handling sotds

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import (
    Blueprint,
    render_template,
)
from werkzeug.exceptions import abort

from ..db import get_db
from .. import orms


bp = Blueprint("sotd", __name__)


@bp.route("/sotd/<int:pk>")
def view(pk):
    db_session = get_db()
    with db_session.begin():
        sotd = db_session.get(orms.SOTD, pk)
        if not sotd:
            abort(404, "SOTD does not exist")
        return render_template("sotd/view.html", sotd=sotd)


@bp.route("/sotd/markdown/<int:sotd_pk>")
def view_markdown(sotd_pk):
    db_session = get_db()
    with db_session.begin():
        sotd = db_session.get(orms.SOTD, sotd_pk)
        if not sotd:
            abort(404, "SOTD does not exist")
        print(sotd.text)
        return render_template("sotd/view_markdown.html", sotd=sotd)


@bp.route("/sotd/<int:user_pk>/<int:challenge_pk>")
def view_sotds_for_user_in_challenge(user_pk, challenge_pk):
    db_session = get_db()
    with db_session.begin():
        user = db_session.get(orms.User, user_pk)
        if user is None:
            abort(404, "User does not exist")
        challenge = db_session.get(orms.ChallengeBase, challenge_pk)
        if challenge is None:
            abort(404, "Challenge does not exist")
        sotd_associations = sorted(
            db_session.query(orms.SOTDAssociation)
            .filter_by(challenge_pk=challenge_pk)
            .join(orms.SOTD, orms.SOTD.pk == orms.SOTDAssociation.sotd_pk)
            .filter_by(user_pk=user_pk),
            key=lambda assoc: assoc.sotd.created_utc,
        )

        return render_template(
            "sotd/view_sotds_for_user_in_challenge.html",
            user=user,
            challenge=challenge,
            sotd_associations=sotd_associations,
        )
