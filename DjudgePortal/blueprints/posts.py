#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   posts.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   16 Aug 2022

@brief  handling posto for a given challeng

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from flask import (
    Blueprint,
    render_template,
)
from werkzeug.exceptions import abort
from .. import orms

from ..db import get_db

bp = Blueprint("post", __name__)


@bp.route("/post/<int:post_pk>/<int:challenge_pk>")
def view(post_pk, challenge_pk):
    db_session = get_db()
    with db_session.begin():
        post = db_session.get(orms.Post, post_pk)
        if not post:
            abort(404, "Post does not exist")
        challenge = db_session.get(orms.ChallengeBase, challenge_pk)
        if not challenge:
            abort(404, "Challenge does not exist")

        sotd_associations = (
            db_session.query(orms.SOTDAssociation)
            .filter_by(challenge_pk=challenge_pk)
            .join(orms.SOTD, orms.SOTD.pk == orms.SOTDAssociation.sotd_pk)
            .filter_by(post_pk=post.pk)
            .order_by(orms.SOTD.created_utc)
            .all()
        )

        return render_template(
            "post/view.html",
            post=post,
            challenge=challenge,
            sotd_associations=sotd_associations,
        )
