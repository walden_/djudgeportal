#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   base.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   14 Apr 2023

@brief  module creating a declarative base class for SQLAlchemy. Offloaded into a file
        to satisfy module level imports at the top of file

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sqlalchemy.orm import DeclarativeBase


class Base(DeclarativeBase):
    pass
