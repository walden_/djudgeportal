#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   __init__.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   02 Jul 2022

@brief  DjudgePortal is a library to help run wetshaving contests

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import os
from flask import Flask

from dotenv import load_dotenv

from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_wtf.csrf import CSRFProtect
from .base import Base
from . import orms
from . import utils

login_manager = LoginManager()
csrf = CSRFProtect()


@login_manager.user_loader
def load_user(user_strpk):
    from . import db

    db_session = db.get_db()
    if db_session().in_transaction():
        return db_session.get(orms.User, int(user_strpk))
    with db_session.begin():
        return db_session.get(orms.User, int(user_strpk))


def create_app(test_config=None, dot_env_file=".env"):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=False)
    conf_path = os.path.join(app.instance_path, dot_env_file)
    load_dotenv(conf_path)
    app.config.from_mapping()

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile(conf_path, silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure that the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import db

    db.init_app(app)
    Bootstrap(app)
    login_manager.init_app(app)
    csrf.init_app(app)

    from . import auth

    app.register_blueprint(auth.bp)

    from .blueprints import (
        challenges,
        users,
        sotds,
        posts,
        djudgements,
        analyses,
        tallies,
        meta_data,
        notes,
    )

    app.register_blueprint(challenges.bp)
    app.register_blueprint(users.bp)
    app.register_blueprint(sotds.bp)
    app.register_blueprint(posts.bp)
    app.register_blueprint(djudgements.bp)
    app.register_blueprint(analyses.bp)
    app.register_blueprint(tallies.bp)
    app.register_blueprint(meta_data.bp)
    app.register_blueprint(notes.bp)
    app.add_url_rule("/", endpoint="index")

    return app


__all__ = ["Base", "load_user", "create_app", "utils"]
