#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   sponsor_points.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   03 Jun 2023

@brief  orm for sponsor point tallying

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from .. import Base

from sqlalchemy import ForeignKey, CheckConstraint, select
from sqlalchemy.orm import Mapped, mapped_column, relationship, object_session
from sqlalchemy.sql.expression import false, true

from typing import List, Optional

from .sotd import SOTD
from .associations import (
    SOTDAssociation,
    SOTDAssociationProperty,
    SOTDAssociationPropertyAssociation,
)
from .user import User

if False:
    from .challenge import ChallengeBase


class Sponsor(Base):
    __tablename__ = "sponsors"
    pk: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(unique=True)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class SponsorSponsorPointAssociation(Base):
    __tablename__ = "sponsor_sponsor_point_association"
    sponsor_pk: Mapped[int] = mapped_column(ForeignKey(Sponsor.pk), primary_key=True)
    sponsor_points_pk: Mapped[int] = mapped_column(
        ForeignKey("sponsor_points.pk"), primary_key=True
    )


class SponsorUse(Base):
    __tablename__ = "sponsor_uses"
    pk: Mapped[int] = mapped_column(primary_key=True)
    sponsor_points_pk: Mapped[int] = mapped_column(ForeignKey("sponsor_points.pk"))
    sponsor_points: Mapped["SponsorPoints"] = relationship()
    sponsor_pk: Mapped[int] = mapped_column(ForeignKey("sponsors.pk"))
    sponsor: Mapped["Sponsor"] = relationship()
    sotd_association_pk: Mapped[int] = mapped_column(ForeignKey("sotd_associations.pk"))
    sotd_association: Mapped["SOTDAssociation"] = relationship(
        back_populates="sponsor_uses"
    )


class SponsorPoints(Base):
    __tablename__ = "sponsor_points"
    pk: Mapped[int] = mapped_column(primary_key=True)

    challenge_pk: Mapped[int] = mapped_column(ForeignKey("challenge_bases.pk"))
    challenge: Mapped["ChallengeBase"] = relationship(back_populates="sponsor_points")
    title: Mapped[str]
    weight: Mapped[float]
    max_score: Mapped[float]
    bonus_for_reaching_max: Mapped[float]

    sponsor_type: Mapped[str] = mapped_column(
        CheckConstraint("sponsor_type in ('hardware', 'software')")
    )

    sponsors: Mapped[List["Sponsor"]] = relationship(
        secondary="sponsor_sponsor_point_association"
    )
    # if property_name is set, then this scavenger hunt is only tallied for
    # sotds for which the property of name `property_name` is True
    property_name: Mapped[Optional[str]]


class SponsorPointsTally(Base):
    __tablename__ = "sponsor_points_tallies"
    pk: Mapped[int] = mapped_column(primary_key=True)

    sponsor_points_pk: Mapped[int] = mapped_column(ForeignKey("sponsor_points.pk"))
    sponsor_points: Mapped["SponsorPoints"] = relationship()

    user_pk: Mapped[int] = mapped_column(ForeignKey("users.pk"))
    user: Mapped["User"] = relationship()

    value: Mapped[float] = mapped_column(default=0.0)

    @property
    def sponsors(self):
        session = object_session(self)
        stmt = (
            select(Sponsor)
            .distinct()
            .join(
                SponsorSponsorPointAssociation,
                SponsorSponsorPointAssociation.sponsor_pk == Sponsor.pk,
            )
            .join(
                SponsorPoints,
                SponsorPoints.pk == SponsorSponsorPointAssociation.sponsor_points_pk,
            )
            .join(SponsorUse, SponsorPoints.pk == SponsorUse.sponsor_points_pk)
            .join(
                SponsorPointsTally,
                SponsorPointsTally.sponsor_points_pk == SponsorPoints.pk,
            )
            .join(SOTDAssociation, SOTDAssociation.pk == SponsorUse.sotd_association_pk)
            .where(SOTDAssociation.challenge_pk == SponsorPoints.challenge_pk)
            .where(SOTDAssociation.challenge_pk == self.sponsor_points.challenge_pk)
            .where(SOTDAssociation.meta_data_checked == true())
            .where(SOTDAssociation.sotd_disqualified == false())
            .join(SOTD, SOTD.pk == SOTDAssociation.sotd_pk)
            .where(Sponsor.pk == SponsorUse.sponsor_pk)
            .where(SOTD.user_pk == self.user_pk)
            .where(SponsorPoints.pk == self.sponsor_points_pk)
        )

        if self.sponsor_points.property_name is not None:
            stmt = (
                stmt.join(
                    SOTDAssociationPropertyAssociation,
                    SOTDAssociationPropertyAssociation.sotd_association_pk
                    == SOTDAssociation.pk,
                )
                .join(
                    SOTDAssociationProperty,
                    SOTDAssociationProperty.pk
                    == SOTDAssociationPropertyAssociation.sotd_association_property_pk,
                )
                .where(
                    SOTDAssociationProperty.name == self.sponsor_points.property_name
                )
                .where(SOTDAssociationPropertyAssociation.value == true())
            )

        return session.execute(stmt).scalars().all()

    def update(self):
        self.value = min(
            self.sponsor_points.max_score,
            self.sponsor_points.weight * len(self.sponsors),
        )
        if self.value == self.sponsor_points.max_score:
            self.value += self.sponsor_points.bonus_for_reaching_max
        return self.value
