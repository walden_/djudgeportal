#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   __init__.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   02 Jul 2022

@brief  object relational mappings for  DjudgePortal

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from .user import User
from .sotd import SOTD, Post
from .score import (
    Category,
    NumericCategory,
    BooleanCategory,
    NoteCategory,
    TallyCategory,
    ConditionalValueCategory,
    MultipleChoiceCategoryChoice,
    MultipleChoiceCategory,
    TextCategory,
    Score,
    NumericScore,
    BooleanScore,
    SOTDTallyScore,
    ConditionalValueScore,
    MultipleChoiceScore,
    TextScore,
    NoteScore,
)
from .analysis import AnalysisBase, SideSideChallengeAnalysis, SOTDPointsTally
from .challenge import (
    DjudgeAssociation,
    ChallengeBase,
    KeywordChallenge,
    ChallengeGroup,
    ChallengeStatus,
)

from .associations import (
    SOTDAssociation,
    PostAssociation,
    SOTDAssociationProperty,
    SOTDAssociationPropertyAssociation,
)

from .djudgement import (
    DjudgementTemplate,
    DjudgementCampaign,
    DjudgementInvitation,
    Djudgement,
    States,
)
from .lathergames2023 import LatherGames2023
from .feats_of_fragrance2023 import FeatsOfFragrance2023
from .photo_contest import Photocontest2023
from .rookie_of_the_year2023 import ROTY2023
from .simple_AA_challenge import AustereAugust

from .sotd_metadata import (
    WSDBRazor,
    WSDBBrush,
    WSDBLather,
    WSDBPostshave,
    WSDBFragrance,
    WSDBHashTag,
    MetaDataItem,
    SOTDMetaDataAssociation,
    HashtagMetaDataAssociation,
)

from .selection import SelectionTemplate, Selection, SelectionTally

from .scavenger_hunt import ScavengerHunt, ScavengerHuntTally
from .sponsor_points import (
    Sponsor,
    SponsorUse,
    SponsorSponsorPointAssociation,
    SponsorPoints,
    SponsorPointsTally,
)

from .meta_data_bonus_points import (
    LatherBrandsBonus,
    LatherScentsBonus,
    PostshaveBonus,
    FragranceBonus,
    UsageBonusTally,
    UsageBonus,
)


__all__ = [
    "DjudgeAssociation",
    "ChallengeBase",
    "KeywordChallenge",
    "SOTDAssociation",
    "ChallengeGroup",
    "ChallengeStatus",
    "PostAssociation",
    "SOTDAssociationProperty",
    "SOTDAssociationPropertyAssociation",
    "User",
    "SOTD",
    "Post",
    "Category",
    "NumericCategory",
    "BooleanCategory",
    "NoteCategory",
    "TallyCategory",
    "ConditionalValueCategory",
    "MultipleChoiceCategoryChoice",
    "MultipleChoiceCategory",
    "TextCategory",
    "Score",
    "NumericScore",
    "BooleanScore",
    "SOTDTallyScore",
    "ConditionalValueScore",
    "MultipleChoiceScore",
    "TextScore",
    "NoteScore",
    "DjudgementTemplate",
    "DjudgementCampaign",
    "DjudgementInvitation",
    "Djudgement",
    "States",
    "AnalysisBase",
    "SideSideChallengeAnalysis",
    "SOTDPointsTally",
    "LatherGames2023",
    "FeatsOfFragrance2023",
    "ROTY2023",
    "AustereAugust",
    "Photocontest2023",
    "WSDBRazor",
    "WSDBBrush",
    "WSDBLather",
    "WSDBPostshave",
    "WSDBFragrance",
    "WSDBHashTag",
    "MetaDataItem",
    "SOTDMetaDataAssociation",
    "HashtagMetaDataAssociation",
    "SelectionTemplate",
    "Selection",
    "SelectionTally",
    "ScavengerHunt",
    "ScavengerHuntTally",
    "Sponsor",
    "SponsorUse",
    "SponsorSponsorPointAssociation",
    "SponsorPoints",
    "SponsorPointsTally",
    "LatherBrandsBonus",
    "LatherScentsBonus",
    "PostshaveBonus",
    "FragranceBonus",
    "UsageBonusTally",
    "UsageBonus",
]
