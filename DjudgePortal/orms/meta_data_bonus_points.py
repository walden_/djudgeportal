#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   meta_data_bonus_points.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   16 Jun 2023

@brief  tallies for meta-data-derived bonus points (e.g., 30 soapers)

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from .. import Base

from sqlalchemy import (
    ForeignKey,
    select,
    func,
    distinct,
    UniqueConstraint,
    CheckConstraint,
)

from sqlalchemy.orm import Mapped, mapped_column, relationship, object_session
from sqlalchemy.sql.expression import false, true

from .sotd_metadata import (
    SOTDMetaDataAssociation,
    WSDBLather,
    WSDBPostshave,
    WSDBFragrance,
)
from .associations import (
    SOTDAssociation,
    SOTDAssociationProperty,
    SOTDAssociationPropertyAssociation,
)
from .sotd import SOTD
from .user import User

if False:
    from .challenge import ChallengeBase


class UsageBonus(Base):
    __tablename__ = "usage_bonus"
    pk: Mapped[int] = mapped_column(primary_key=True)
    bonus_types = (
        "'LatherBrandBonus', 'LatherScentBonus', 'PostshaveBonus', 'FragranceBonus'"
    )

    challenge_pk: Mapped[int] = mapped_column(ForeignKey("challenge_bases.pk"))
    challenge: Mapped["ChallengeBase"] = relationship()
    title: Mapped[str] = mapped_column()
    threshold: Mapped[int]
    bonus: Mapped[float]

    usage_bonus_type: Mapped[str] = mapped_column(
        CheckConstraint(f"usage_bonus_type in ({bonus_types})")
    )

    __table_args__ = (UniqueConstraint(title, challenge_pk),)
    __mapper_args__ = {
        "polymorphic_identity": "BASE",
        "polymorphic_on": usage_bonus_type,
        "with_polymorphic": "*",
    }

    def use_count(self, user):
        """
        return the count of lather brands used by user user in challenge self.challenge
        Keyword Arguments:
        user -- user for whom to tally the use count
        """
        sess = object_session(self)
        orm, col = self.countable
        stmt = (
            select(func.count(distinct(col)))
            .join(
                SOTDMetaDataAssociation,
                SOTDMetaDataAssociation.metadata_item_pk == orm.pk,
            )
            .where(SOTDMetaDataAssociation.djudicial_dq.is_(False))
            .join(
                SOTDAssociation,
                SOTDAssociation.pk == SOTDMetaDataAssociation.sotd_association_pk,
            )
            .where(SOTDAssociation.meta_data_checked == true())
            .where(SOTDAssociation.challenge_pk == self.challenge_pk)
            .where(SOTDAssociation.sotd_disqualified == false())
            .join(
                SOTDAssociationPropertyAssociation,
                SOTDAssociationPropertyAssociation.sotd_association_pk
                == SOTDAssociation.pk,
            )
            .where(SOTDAssociationPropertyAssociation.value == true())
            .join(
                SOTDAssociationProperty,
                SOTDAssociationProperty.pk
                == SOTDAssociationPropertyAssociation.sotd_association_property_pk,
            )
            .where(SOTDAssociationProperty.name == "on theme")
            .join(SOTD, SOTD.pk == SOTDAssociation.sotd_pk)
            .join(User, User.pk == SOTD.user_pk)
            .where(User.pk == user.pk)
        )
        result = sess.execute(stmt).one()[0]
        return result


class LatherBrandsBonus(UsageBonus):
    __mapper_args__ = {
        "polymorphic_identity": "LatherBrandBonus",
    }

    @property
    def countable(self):
        return WSDBLather, WSDBLather.brand


class LatherScentsBonus(UsageBonus):
    __mapper_args__ = {
        "polymorphic_identity": "LatherScentBonus",
    }

    @property
    def countable(self):
        return WSDBLather, WSDBLather.pk


class PostshaveBonus(UsageBonus):
    __mapper_args__ = {
        "polymorphic_identity": "PostshaveBonus",
    }

    @property
    def countable(self):
        return WSDBPostshave, WSDBPostshave.pk


class FragranceBonus(UsageBonus):
    __mapper_args__ = {
        "polymorphic_identity": "FragranceBonus",
    }

    @property
    def countable(self):
        return WSDBFragrance, WSDBFragrance.pk


class UsageBonusTally(Base):
    __tablename__ = "usage_bonus_tallies"
    pk: Mapped[int] = mapped_column(primary_key=True)

    usage_bonus_pk: Mapped[int] = mapped_column(ForeignKey("usage_bonus.pk"))
    usage_bonus: Mapped["UsageBonus"] = relationship()

    user_pk: Mapped[int] = mapped_column(ForeignKey("users.pk"))
    user: Mapped["User"] = relationship()

    value: Mapped[float] = mapped_column(default=0.0)

    @property
    def use_count(self):
        return self.usage_bonus.use_count(self.user)

    def update(self):
        if self.use_count >= self.usage_bonus.threshold:
            self.value = self.usage_bonus.bonus
        else:
            self.value = 0.0
        return self.value
