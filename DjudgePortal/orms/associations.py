#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   associations.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   10 Jun 2023

@brief  Some association tables are moved here to deal with circular imports

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sqlalchemy import (
    Column,
    Integer,
    Boolean,
    ForeignKey,
    UniqueConstraint,
)
from sqlalchemy.orm import (
    relationship,
    attribute_keyed_dict,
    Mapped,
    mapped_column,
    object_session,
)

from typing import Optional

from .. import Base


class SOTDAssociationProperty(Base):
    __tablename__ = "sotd_association_properties"
    pk: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(unique=True)


class SOTDAssociationPropertyAssociation(Base):
    __tablename__ = "sotd_association_property_associations"
    sotd_association_property_pk: Mapped[int] = mapped_column(
        ForeignKey("sotd_association_properties.pk"), primary_key=True
    )
    sotd_association_property: Mapped[SOTDAssociationProperty] = relationship()
    sotd_association_pk: Mapped[int] = mapped_column(
        ForeignKey("sotd_associations.pk"), primary_key=True
    )
    sotd_association: Mapped["SOTDAssociation"] = relationship(
        back_populates="properties"
    )
    value: Mapped[Optional[bool]]

    @property
    def name(self):
        return self.sotd_association_property.name

    def __repr__(self):
        return (
            f"Property(name: '{self.name}', value: '{self.value}') for SOTD "
            f"association {self.sotd_association}."
        )


class SOTDAssociation(Base):
    __tablename__ = "sotd_associations"
    pk = Column(Integer, primary_key=True)
    challenge_pk = Column(Integer, ForeignKey("challenge_bases.pk"))
    challenge = relationship("ChallengeBase", viewonly=True)
    sotd_pk = Column(Integer, ForeignKey("sotds.pk"))
    sotd = relationship("SOTD", viewonly=True)
    djudge_pk = Column(ForeignKey("users.pk"))
    djudge = relationship("User")
    meta_data_checked = Column(Boolean, nullable=False, default=False)
    sotd_disqualified = Column(Boolean, nullable=False, default=False)
    djudgement = relationship(
        "Djudgement", secondary="djudgement_invitations", viewonly=True, uselist=False
    )

    __table_args__ = (UniqueConstraint("challenge_pk", "sotd_pk"),)

    properties = relationship(
        SOTDAssociationPropertyAssociation,
        collection_class=attribute_keyed_dict("name"),
        back_populates="sotd_association",
    )

    notes = relationship(
        "NoteScore",
        viewonly=True,
        secondaryjoin=(
            "and_(NoteScore.djudgement_pk == Djudgement.pk,"
            "     NoteScore._value != '')"
        ),
        secondary=(
            "join(Djudgement, DjudgementInvitation,"
            "     Djudgement.djudgement_invitation_pk =="
            "     DjudgementInvitation.pk)"
        ),
        primaryjoin="SOTDAssociation.pk == DjudgementInvitation.sotd_association_pk",
    )

    @property
    def post_association(self):
        session = object_session(self)
        return (
            session.query(PostAssociation)
            .filter_by(challenge=self.challenge, post=self.sotd.post)
            .one()
        )

    def get_property(self, property_name):
        if property_name not in self.properties.keys():
            prop = (
                object_session(self)
                .query(SOTDAssociationProperty)
                .filter_by(name=property_name)
                .one_or_none()
            )
            if prop is None:
                prop = SOTDAssociationProperty(name=property_name)
            self.properties[property_name] = SOTDAssociationPropertyAssociation(
                sotd_association_property=prop,
                sotd_association=self,
            )
            object_session(self).flush()
        return self.properties[property_name]

    def property_value(self, property_name):
        if property_name in self.properties.keys():
            print(f"property_value = {self.properties[property_name]}")
            return self.properties[property_name]
        return None

    sponsor_uses = relationship(
        "SponsorUse",
        collection_class=attribute_keyed_dict(
            "sponsor_pk", ignore_unpopulated_attribute=True
        ),
        back_populates="sotd_association",
    )

    __meta_info = relationship(
        "SOTDMetaDataAssociation",
        viewonly=True,
    )

    @property
    def meta_info(self):
        cat_val = {
            "Razor": 0,
            "Brush": 1,
            "Lather": 2,
            "Post Shave": 3,
            "Fragrance": 4,
        }
        return sorted(
            self.__meta_info,
            key=lambda item: (cat_val[item.item.category_name], item.pk),
        )


class PostAssociation(Base):
    __tablename__ = "post_associations"
    pk = Column(Integer, primary_key=True)
    challenge_pk = Column(Integer, ForeignKey("challenge_bases.pk"), nullable=False)
    challenge = relationship("ChallengeBase", back_populates="post_associations")
    post_pk = Column(Integer, ForeignKey("posts.pk"), nullable=False)
    post = relationship("Post", back_populates="challenge_associations")
    day = Column(Integer, nullable=False)
    __table_args__ = (
        UniqueConstraint("challenge_pk", "post_pk"),
        UniqueConstraint("challenge_pk", "day"),
    )
