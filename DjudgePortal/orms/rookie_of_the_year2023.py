#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   rookie_of_the_year2023.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   26 Jun 2023

@brief  Rookie of the year 2023

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from . import KeywordChallenge

from .score import ConditionalValueCategory

from .djudgement import DjudgementTemplate


class ROTY2023(KeywordChallenge):
    __mapper_args__ = {
        "polymorphic_identity": "ROTY2023",
    }

    def fetch_or_create_category(self, db_session):
        title = "Counts"
        category = (
            db_session.query(ConditionalValueCategory)
            .filter_by(title=title)
            .filter_by(challenge_pk=self.pk)
            .one_or_none()
        )
        if category is None:
            category = ConditionalValueCategory(
                title=title,
                guidance="Check this box if this is a valid ROTY post",
                challenge=self,
                weight=1.0,
            )
            db_session.add(category)
            db_session.flush()
        return category

    def create_invite(self, db_session):
        name = "Invitation"
        template = DjudgementTemplate(
            challenge=self,
            categories=[self.fetch_or_create_category(db_session)],
            name=name,
        )
        self.djudgement_templates[name] = template
        return template
