#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   djudgement.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   17 Aug 2022

@brief  orm for djudgements and djudgement invitations

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sqlalchemy import (
    Column,
    Integer,
    Enum,
    ForeignKey,
    Unicode,
    func,
    UnicodeText,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship, attribute_keyed_dict, object_session
from sqlalchemy.types import DateTime

import markdown
from markdown.extensions.tables import TableExtension

import enum
from .. import Base


@enum.unique
class States(enum.Enum):
    Unacknowledged = 1
    Djudged = 2


class DjudgementTemplate(Base):
    """
    Specifies a set of categories from which to generate `DjudgementInvitation`s
    """

    __tablename__ = "djudgement_templates"
    pk = Column(Integer, primary_key=True)
    challenge_pk = Column(ForeignKey("challenge_bases.pk"), nullable=False)
    challenge = relationship("ChallengeBase", back_populates="djudgement_templates")
    categories = relationship(
        "Category",
        back_populates="djudgement_templates",
        secondary="template_associations",
    )
    djudgement_campaigns = relationship("DjudgementCampaign", back_populates="template")
    name = Column(Unicode, nullable=False)
    __table_args__ = (UniqueConstraint("challenge_pk", "name"),)


class DjudgementCampaign(Base):
    """
    A specific implementation of a DjudgementTemplate with intructions for djudges
    (think a specific day of Lather Games: It has a certain set of Categories
    (Template), and a daily theme and challenge (Instructions for djudges)
    """

    __tablename__ = "djudgement_campaigns"
    pk = Column(Integer, primary_key=True)
    template_pk = Column(ForeignKey("djudgement_templates.pk"), nullable=False)
    template = relationship("DjudgementTemplate", back_populates="djudgement_campaigns")
    invitations = relationship("DjudgementInvitation", viewonly=True)
    instructions = Column(UnicodeText, nullable=False)

    def unambiguously_determine_post_association(self):
        post_associations = {
            inv.sotd_association.post_association for inv in self.invitations
        }
        if not post_associations:
            return None
        elif len(post_associations) > 1:
            raise RuntimeError(
                "Can't determine unambiguous post association. I should have "
                f"found zero or one, but found {len(post_associations)}."
            )

        return post_associations.pop()

    @property
    def instructions_markdown(self):
        return markdown.markdown(self.instructions, extensions=[TableExtension()])

    def invite_sotd(self, *, sotd, djudge=None):
        sotd_association = self.template.challenge.sotd_associations[sotd]
        if djudge is None and sotd_association.djudge is None:
            raise ValueError(
                "you need to specify a djudge if there isn't already one assigned."
            )
        elif (
            djudge is not None
            and sotd_association.djudge is not None
            and sotd_association.djudge != djudge
        ):
            raise ValueError(
                f"SOTD '{sotd}' was already assigned djudge "
                f"'{sotd_association.djudge}', and you're trying to assign djudge "
                f"'{djudge}'."
            )
        if sotd_association.djudge is None:
            sotd_association.djudge = djudge
        return DjudgementInvitation(
            sotd_association=sotd_association,
            categories=self.template.categories,
            campaign=self,
        )


class DjudgementInvitation(Base):
    States = States
    __tablename__ = "djudgement_invitations"
    pk = Column(Integer, primary_key=True)
    issued = Column(DateTime(timezone=True), server_default=func.now())
    sotd_association_pk = Column(ForeignKey("sotd_associations.pk"), nullable=False)
    sotd_association = relationship("SOTDAssociation")

    @property
    def challenge(self):
        return self.sotd_association.challenge

    @property
    def djudge(self):
        return self.sotd_association.djudge

    @property
    def djudge_pk(self):
        return self.sotd_association.djudge_pk

    state = Column(Enum(States), nullable=False, default=States.Unacknowledged)
    categories = relationship(
        "Category",
        back_populates="djudgement_invitations",
        secondary="category_associations",
    )
    djudgement = relationship(
        "Djudgement", back_populates="djudgement_invitation", uselist=False
    )
    djudgement_type = Column(Unicode, nullable=False)
    __mapper_args__ = {
        "polymorphic_identity": "DjudgementInvitation",
        "polymorphic_on": djudgement_type,
        "with_polymorphic": "*",
    }

    @property
    def sotd(self):
        return self.sotd_association.sotd

    campaign_pk = Column(ForeignKey("djudgement_campaigns.pk"))
    campaign = relationship("DjudgementCampaign")

    def __init__(self, sotd_association, campaign=None, categories=None):
        if sotd_association.djudge is None:
            raise ValueError(
                f"Association '{sotd_association}' was not assigned a djudge yet"
            )
        self.sotd_association = sotd_association

        self.campaign = campaign if campaign else self.campaign
        self.categories = categories if categories else self.categories

    @property
    def user(self):
        return self.sotd.author


class Djudgement(Base):
    __tablename__ = "djudgements"

    def __init__(self, djudgement_invitation):
        """overridden default init to assure user is assigned correctly. It's redundant
        information whch could conceivably go out of sync

        Keyword Arguments:
        djudgement_invitation -- The invide
        scores                -- (default None)

        """
        self.user = djudgement_invitation.user
        self.djudgement_invitation = djudgement_invitation

    pk = Column(Integer, primary_key=True)
    user_pk = Column(ForeignKey("users.pk"), nullable=False)
    user = relationship("User")
    djudgement_invitation_pk = Column(
        ForeignKey("djudgement_invitations.pk"), nullable=False
    )
    djudgement_invitation = relationship(
        "DjudgementInvitation",
        back_populates="djudgement",
    )
    num_scores = relationship("ConditionalValueScore", viewonly=True)
    scores = relationship(
        "Score",
        back_populates="djudgement",
    )
    dq_scores = relationship("DisqualificationScore", viewonly=True)
    score_dict = relationship(
        "ConditionalValueScore",
        collection_class=attribute_keyed_dict("title"),
        back_populates="djudgement",
        viewonly=True,
    )

    def DQed(self):
        return self.disqualified

    @property
    def disqualified(self):
        disqualified = any((s.disqualified for s in self.dq_scores))
        if (
            disqualified
            != self.djudgement_invitation.sotd_association.sotd_disqualified
        ):
            self.djudgement_invitation.sotd_association.sotd_disqualified = disqualified
            object_session(self).flush()
        return disqualified
