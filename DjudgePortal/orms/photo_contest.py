#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   photo_contest.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   22 Jun 2023

@brief  orms relevant to u/Enndeegee's and u/Semaj3000's photo contest

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

tts2backend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tts2backend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from . import KeywordChallenge

from .score import MultipleChoiceCategory, MultipleChoiceCategoryChoice, NoteCategory

from .djudgement import DjudgementTemplate


class Photocontest2023(KeywordChallenge):
    __mapper_args__ = {
        "polymorphic_identity": "Photocontest2023",
    }

    def fetch_or_create_category(self, db_session):
        title = "Score"
        score_category = (
            db_session.query(MultipleChoiceCategory)
            .filter_by(title=title)
            .filter_by(challenge_pk=self.pk)
            .one_or_none()
        )
        if score_category is None:
            score_category = MultipleChoiceCategory(
                title=title,
                guidance=(
                    "I've had a stupid idea for a photo contest for lather games "
                    "as it seems a bit quiet on the side contest front. basically "
                    "its a scavenger hunt. 30 topics and if you get them in your "
                    "SOTD photo you get points, 1 point for a picture of the thing, "
                    "5 points for a model version, 10 for the real thing and 30 for "
                    "the relevant user."
                ),
                challenge=self,
                choices=[
                    MultipleChoiceCategoryChoice(label="nothing", weight=0.0),
                    MultipleChoiceCategoryChoice(
                        label="picture of the thing", weight=1
                    ),
                    MultipleChoiceCategoryChoice(label="model version", weight=5),
                    MultipleChoiceCategoryChoice(label="real thing", weight=10),
                    MultipleChoiceCategoryChoice(label="relevant user", weight=30),
                ],
            )
            db_session.add(score_category)
            db_session.flush()

        title = "Candidate for best shot"
        note_category = (
            db_session.query(NoteCategory)
            .filter_by(title=title)
            .filter_by(challenge_pk=self.pk)
            .one_or_none()
        )
        if note_category is None:
            note_category = NoteCategory(
                title=title,
                guidance=(
                    "If you think this is a candidate for best picture, explain "
                    "why. Warning, this is visible to participants. (Markdown allowed)"
                ),
                property_name="note_best_picture",
                challenge=self,
            )
            db_session.add(note_category)
            db_session.flush()

        return [score_category, note_category]

    def create_invite(self, db_session):
        name = "Invitation"
        template = DjudgementTemplate(
            challenge=self,
            categories=self.fetch_or_create_category(db_session),
            name=name,
        )
        self.djudgement_templates[name] = template
        return template
