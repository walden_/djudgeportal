#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   challenge.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   15 Jul 2022

@brief  ORM for challenges

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from sqlalchemy import (
    Column,
    Integer,
    Enum,
    Unicode,
    Boolean,
    ForeignKey,
    UnicodeText,
    Table,
    select,
)
from .associations import SOTDAssociation, PostAssociation

from typing import List, Dict
from sqlalchemy.orm import (
    relationship,
    Mapped,
    column_keyed_dict,
    attribute_keyed_dict,
    object_session,
)
import enum

from ..utils import get_user


import markdown
from markdown.extensions.tables import TableExtension
from datetime import datetime
from collections import defaultdict

from .. import Base
from .sotd import SOTD
from .score import TallyCategory, Category, NoteScore, SOTDTallyScore
from .djudgement import Djudgement, DjudgementInvitation, DjudgementTemplate
from .analysis import SOTDPointsTally
from .selection import SelectionTally, SelectionTemplate
from .user import User
from .scavenger_hunt import ScavengerHuntTally, ScavengerHunt
from .sponsor_points import SponsorPoints, SponsorPointsTally
from .meta_data_bonus_points import UsageBonus, UsageBonusTally


_ = Table(
    "owner_association",
    Base.metadata,
    Column("challenge_pk", ForeignKey("challenge_bases.pk"), primary_key=True),
    Column("owner_pk", ForeignKey("users.pk"), primary_key=True),
)


class DjudgeAssociation(Base):
    __tablename__ = "djudge_association"
    challenge_pk = Column(ForeignKey("challenge_bases.pk"), primary_key=True)
    challenge = relationship("ChallengeBase", viewonly=True)
    djudge_pk = Column(ForeignKey("users.pk"), primary_key=True)
    djudge = relationship("User", viewonly=True)
    active = Column(Boolean, nullable=False, default=True)


@enum.unique
class ChallengeStatus(enum.Enum):
    Inactive = 1
    Active = 2
    Concluded = 3


class ChallengeBase(Base):
    __tablename__ = "challenge_bases"
    pk = Column(Integer, primary_key=True)
    name = Column(UnicodeText, nullable=False, unique=True)
    title = Column(UnicodeText, nullable=False, unique=True, default=name)
    post_url = Column(UnicodeText, nullable=False, default=name)
    description = Column(UnicodeText, nullable=False)
    owners = relationship(
        "User",
        secondary="owner_association",
        back_populates="challenges_owned",
        order_by="User.user_name_lower",
    )
    participants = relationship(
        "User",
        secondary=(
            "join(SOTD, User)"
            ".join(SOTDAssociation, SOTDAssociation.sotd_pk == SOTD.pk)"
        ),
        primaryjoin=(
            "and_(ChallengeBase.pk == SOTDAssociation.challenge_pk, "
            "SOTDAssociation.sotd_pk == SOTD.pk)"
        ),
        secondaryjoin="User.pk == SOTD.user_pk",
        viewonly=True,
        order_by="User.user_name_lower",
    )
    djudges = relationship(
        "User",
        secondary="djudge_association",
        back_populates="challenges_djudged",
        order_by="User.user_name_lower",
    )
    djudge_associations = relationship("DjudgeAssociation", viewonly=True)

    @property
    def active_djudges(self):
        return {assoc.djudge for assoc in self.djudge_associations if assoc.active}

    categories: Mapped[Dict[str, "Category"]] = relationship(
        "Category",
        back_populates="challenge",
        collection_class=column_keyed_dict(
            Category.title, ignore_unpopulated_attribute=True
        ),
        cascade="all, delete-orphan",
    )
    sotds = relationship(
        "SOTD", secondary="sotd_associations", back_populates="challenges"
    )
    posts = relationship(
        "Post",
        secondary="post_associations",
        back_populates="challenges",
        viewonly=True,
        order_by="Post.pk",
    )

    current_post_pk = Column(ForeignKey("posts.pk"))
    current_post = relationship("Post")
    post_associations = relationship(PostAssociation, back_populates="challenge")
    sotd_associations = relationship(
        SOTDAssociation,
        collection_class=attribute_keyed_dict("sotd"),
        back_populates="challenge",
        viewonly=True,
    )
    challenge_groups = relationship(
        "ChallengeGroup",
        secondary="group_associations",
        back_populates="challenges",
    )

    status = Column(
        Enum(ChallengeStatus), nullable=False, default=ChallengeStatus.Inactive
    )

    def compute_djudge_points(self):
        """
        return the number numeric value of points given by any djudge
        """
        session = object_session(self)
        stmt = (
            select(SOTDTallyScore, Djudgement, DjudgementInvitation, User)
            .select_from(SOTDTallyScore)
            .join(Category, SOTDTallyScore.category_pk == Category.pk)
            .join(Djudgement, SOTDTallyScore.djudgement_pk == Djudgement.pk)
            .join(
                DjudgementInvitation,
                DjudgementInvitation.pk == Djudgement.djudgement_invitation_pk,
            )
            .join(
                SOTDAssociation,
                SOTDAssociation.pk == DjudgementInvitation.sotd_association_pk,
            )
            .join(User, User.pk == SOTDAssociation.djudge_pk)
            .where(Category.challenge_pk == self.pk)
        )
        scores = session.execute(stmt)

        points = defaultdict(float)
        for score, _, _, djudge in scores:
            points[djudge] += score.num_val

        return points

    def count_djudgements(self):
        nb_djudgements = defaultdict(int)
        nb_dqs = defaultdict(int)
        for assoc in self.sotd_associations.values():
            nb_djudgements[assoc.djudge] += 1
            if assoc.sotd_disqualified:
                nb_dqs[assoc.djudge] += 1
        return nb_djudgements, nb_dqs

    def currently_active(self):
        return self.status == ChallengeStatus.Active

    djudgement_invitations = relationship(
        "DjudgementInvitation", secondary="sotd_associations", viewonly=True
    )
    djudgement_templates = relationship(
        "DjudgementTemplate",
        collection_class=column_keyed_dict(
            DjudgementTemplate.name, ignore_unpopulated_attribute=True
        ),
        cascade="all, delete-orphan",
    )
    djudgements = relationship(
        "Djudgement",
        secondary=(
            "join(DjudgementInvitation, SOTDAssociation,"
            "     DjudgementInvitation.sotd_association_pk =="
            "     SOTDAssociation.pk)"
        ),
        viewonly=True,
    )

    selection_templates = relationship(
        "SelectionTemplate",
        collection_class=column_keyed_dict(
            SelectionTemplate.title, ignore_unpopulated_attribute=True
        ),
        back_populates="challenge",
    )

    analyses = relationship(
        "AnalysisBase",
        secondary="challenge_association",
        back_populates="challenges",
    )
    challenge_type = Column(Unicode, nullable=False)

    sotd_points_tallies: Mapped[List["SOTDPointsTally"]] = relationship(
        back_populates="challenge"
    )
    selection_tallies: Mapped[List["SelectionTally"]] = relationship(
        secondary=(
            "join(SelectionTally, SelectionTemplate, "
            "     SelectionTally.selection_template_pk == SelectionTemplate.pk)"
        ),
        primaryjoin="SelectionTemplate.challenge_pk == ChallengeBase.pk",
        viewonly=True,
    )

    scavenger_hunts: Mapped[List["ScavengerHunt"]] = relationship(
        back_populates="challenge"
    )

    razor_scavenger_hunts: Mapped[List["ScavengerHunt"]] = relationship(
        primaryjoin=(
            "and_(ScavengerHunt.challenge_pk == ChallengeBase.pk, "
            "     ScavengerHunt.metadata_type == 'razor')"
        ),
        viewonly=True,
    )

    brush_scavenger_hunts: Mapped[List["ScavengerHunt"]] = relationship(
        primaryjoin=(
            "and_(ScavengerHunt.challenge_pk == ChallengeBase.pk, "
            "     ScavengerHunt.metadata_type == 'brush')"
        ),
        viewonly=True,
    )

    sponsor_points: Mapped[List[SponsorPoints]] = relationship(
        back_populates="challenge"
    )

    usage_boni: Mapped[List[UsageBonus]] = relationship(back_populates="challenge")

    __mapper_args__ = {
        "polymorphic_identity": "ChallengeBase",
        "polymorphic_on": challenge_type,
        "with_polymorphic": "*",
    }

    note_list: Mapped[List[NoteScore]] = relationship(
        viewonly=True,
        secondaryjoin=(
            "and_(NoteScore.djudgement_pk == Djudgement.pk,"
            "     NoteScore._value != '')"
        ),
        secondary=(
            "join(Djudgement, DjudgementInvitation,"
            "     Djudgement.djudgement_invitation_pk =="
            "     DjudgementInvitation.pk)"
            ".join(SOTDAssociation,"
            "      SOTDAssociation.pk =="
            "      DjudgementInvitation.sotd_association_pk)"
        ),
        primaryjoin="SOTDAssociation.challenge_pk == ChallengeBase.pk",
    )

    @property
    def notes(self):
        keys = {note.property_name for note in self.note_list}
        return {
            key: [note for note in self.note_list if note.property_name == key]
            for key in keys
        }

    @property
    def has_meta_data_check(self):
        "whether djudges should validate meta_data before adjudication"
        return False

    @property
    def meta_data_checks(self):
        "Which meta data items to check"
        return ()

    def __iter__(self):
        yield "name", self.name
        yield "description", self.description
        yield "categories", self.categories
        yield "owners", self.owners
        yield "djudges", self.djudges
        yield "title", self.title
        yield "post_url", self.post_url
        yield "status", self.status

    @property
    def description_markdown(self):
        return markdown.markdown(self.description, extensions=[TableExtension()])

    def scan_current_post(self, additional_filter=lambda x: True):
        for sotd in self.current_post.sotds:
            self.handle_new_comment(
                session=object_session(self),
                post=None,
                comment=sotd,
                additional_filter=additional_filter,
            )

    def handle_new_comment(
        self, session, post, comment, additional_filter=lambda x: True, override=False
    ):
        """
        check whether this comment is relevant and if so, ingest it and return it
        as SOTD, else return None

        Keyword Arguments:
        session -- sqlalchemy db session
        post    -- orm.Post in which this SOTD is written
        comment -- praw reddit comment or DjudgePortal.orms.SOTD to check
        additional_filter -- an ad hoc filter that can additionally exclude sotds
        override -- add sotd even if participation checks fail
        """
        if isinstance(comment, SOTD):
            if override or (
                additional_filter(comment.text)
                and self.check_for_participation(comment.text)
            ):
                if comment not in self.sotds:
                    self.sotds.append(comment)
                return comment
            return None

        if override or (
            additional_filter(comment.body)
            and self.check_for_participation(comment.body)
        ):
            sotd = (
                session.query(SOTD).filter_by(permalink=comment.permalink).one_or_none()
            )
            if sotd is not None:
                self.sotds.append(sotd)
                return sotd
            if sotd is None:
                user = get_user(session, comment.author)
                if user:
                    sotd = SOTD(
                        author=user,
                        created_utc=comment.created_utc,
                        post_pk=post.pk,
                        retrieved_on=datetime.now().timestamp(),
                        text=comment.body,
                        permalink=comment.permalink,
                    )
                    self.sotds.append(sotd)
                    return sotd
        return None

    def update_tally_for_user(self, author, db_session):
        """
        implement running tally for Lather Games 2023 Djudge points
        Keyword Arguments:
        author     -- participant
        db_session -- sqlalchemy session
        """

        # make sure there's a tally object for this user in this challenge
        def fetch_or_create_sotd_tally(category):
            tally = (
                db_session.query(SOTDPointsTally)
                .filter_by(category_pk=category.pk)
                .filter_by(challenge_pk=self.pk)
                .filter_by(user_pk=author.pk)
            ).one_or_none()
            if tally is None:
                tally = SOTDPointsTally(
                    category=category, challenge=self, user=author, value=0.0
                )
                self.sotd_points_tallies.append(tally)
                db_session.flush()  # required for subsequent update to work
            return tally

        stmt = select(TallyCategory).where(TallyCategory.challenge_pk == self.pk)
        val_categories = db_session.scalars(stmt).all()
        for category in val_categories:
            fetch_or_create_sotd_tally(category).update()

        def fetch_or_create_selection_tally(selection_template):
            tally = (
                db_session.query(SelectionTally)
                .filter_by(user_pk=author.pk)
                .filter_by(selection_template_pk=selection_template.pk)
                .one_or_none()
            )
            if tally is None:
                tally = SelectionTally(
                    value=0.0, user=author, selection_template=selection_template
                )
                db_session.add(tally)
                db_session.flush()
                db_session.refresh(self)
            return tally

        for selection_template in self.selection_templates.values():
            fetch_or_create_selection_tally(selection_template).update()

        def fetch_or_create_scavenger_hunt_tally(scavenger_hunt):
            tally = (
                db_session.query(ScavengerHuntTally)
                .filter_by(user_pk=author.pk)
                .filter_by(scavenger_hunt_pk=scavenger_hunt.pk)
                .one_or_none()
            )
            if tally is None:
                tally = ScavengerHuntTally(
                    value=0.0, user=author, scavenger_hunt=scavenger_hunt
                )
                db_session.add(tally)
                db_session.flush()
                db_session.refresh(self)
            return tally

        for scavenger_hunt in self.scavenger_hunts:
            fetch_or_create_scavenger_hunt_tally(scavenger_hunt).update()

        def fetch_or_create_sponsor_tally(sponsor_points):
            tally = (
                db_session.query(SponsorPointsTally)
                .filter_by(user_pk=author.pk)
                .filter_by(sponsor_points=sponsor_points)
                .one_or_none()
            )
            if tally is None:
                tally = SponsorPointsTally(sponsor_points=sponsor_points, user=author)
                db_session.add(tally)
                db_session.flush()
                db_session.refresh(self)
            return tally

        for sponsor_points in self.sponsor_points:
            fetch_or_create_sponsor_tally(sponsor_points).update()

        def fetch_or_create_bonus_tally(usage_bonus):
            tally = (
                db_session.query(UsageBonusTally)
                .filter_by(user_pk=author.pk)
                .filter_by(usage_bonus=usage_bonus)
                .one_or_none()
            )
            if tally is None:
                tally = UsageBonusTally(usage_bonus=usage_bonus, user=author)
                db_session.add(tally)
                db_session.flush()
                db_session.refresh(self)
            return tally

        for usage_bonus in self.usage_boni:
            fetch_or_create_bonus_tally(usage_bonus).update()


_ = Table(
    "group_associations",
    Base.metadata,
    Column("challenge_pk", ForeignKey("challenge_bases.pk"), primary_key=True),
    Column("challenge_group_pk", ForeignKey("challenge_groups.pk"), primary_key=True),
)


class KeywordChallenge(ChallengeBase):
    """
    simple challenge where participanting SOTDs contain a keyword (e.g., like
    GEMsOfWisdom)
    """

    __tablename__ = "keyword_challenges"
    pk = Column(Integer, ForeignKey("challenge_bases.pk"), primary_key=True)
    search_string = Column(UnicodeText, nullable=False)

    __mapper_args__ = {
        "polymorphic_identity": "KeywordChallenge",
    }

    def check_for_participation(self, body):
        return self.search_string.lower() in body.lower()

    def __iter__(self):
        yield "name", self.name
        yield "description", self.description
        yield "categories", self.categories
        yield "owners", self.owners
        yield "djudges", self.djudges
        yield "title", self.title
        yield "post_url", self.post_url
        yield "search_string", self.search_string
        yield "is_active", self.is_active


class ChallengeGroup(Base):
    __tablename__ = "challenge_groups"
    pk = Column(Integer, primary_key=True)
    name = Column(Unicode, nullable=False, unique=True)
    next_post_url = Column(Unicode, nullable=True)
    alert_djudges = Column(Boolean, nullable=False, default=False)
    challenges = relationship(
        "ChallengeBase",
        secondary="group_associations",
        back_populates="challenge_groups",
    )

    def __iter__(self):
        yield "next_post_url", self.next_post_url
        yield "alert_djudges", self.alert_djudges
