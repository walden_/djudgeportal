#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   score.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   02 Jul 2022

@brief  ORM for scores

Copyright © 2022 Djundjila

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from sqlalchemy import (
    Column,
    Integer,
    Float,
    Boolean,
    Unicode,
    ForeignKey,
    UnicodeText,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship, column_keyed_dict
import markdown
from markdown.extensions.tables import TableExtension

from .. import Base


class CategoryAssociation(Base):
    __tablename__ = "category_associations"
    djudgement_invitation_pk = Column(
        ForeignKey("djudgement_invitations.pk"), primary_key=True
    )
    category_pk = Column(ForeignKey("categories.pk"), primary_key=True)


class TemplateAssociation(Base):
    __tablename__ = "template_associations"
    djudgement_template_pk = Column(
        ForeignKey("djudgement_templates.pk"), primary_key=True
    )
    category_pk = Column(ForeignKey("categories.pk"), primary_key=True)


class Category(Base):
    __tablename__ = "categories"
    pk = Column(Integer, primary_key=True)
    title = Column(UnicodeText, nullable=False)
    guidance = Column(UnicodeText, nullable=False)
    scores = relationship("Score", back_populates="category")
    challenge_pk = Column(Integer, ForeignKey("challenge_bases.pk"), nullable=False)
    challenge = relationship("ChallengeBase", back_populates="categories")
    djudge_is_eligible = Column(Boolean, nullable=False, default=False)
    djudgement_templates = relationship(
        "DjudgementTemplate",
        back_populates="categories",
        secondary="template_associations",
    )
    djudgement_invitations = relationship(
        "DjudgementInvitation",
        back_populates="categories",
        secondary="category_associations",
    )
    category_type = Column(Unicode, nullable=False)
    __table_args__ = (UniqueConstraint("challenge_pk", "title"),)

    __mapper_args__ = {
        "polymorphic_identity": "Category",
        "polymorphic_on": category_type,
        "with_polymorphic": "*",
    }

    @property
    def djudicial_eligibility_disclaimer(self):
        if not self.djudge_is_eligible:
            return ", djudges not eligible"
        return ""

    def __repr__(self):
        return (
            f"{self.title} ({self.category_type}"
            f"{self.djudicial_eligibility_disclaimer})"
        )

    @property
    def guidance_markdown(self):
        return markdown.markdown(self.guidance, extensions=[TableExtension()])

    def new_score(self, value, djudgement):
        return self.score_cls(djudgement=djudgement, value=value, category=self)


class NumericCategory(Category):
    __tablename__ = "numeric_categories"
    pk = Column(Integer, ForeignKey("categories.pk"), primary_key=True)
    minimum = Column(Float, nullable=False, default=0)
    maximum = Column(Float, nullable=False, default=100)
    default = Column(Float)
    __mapper_args__ = {
        "polymorphic_identity": "NumericCategory",
    }

    @property
    def score_cls(self):
        return NumericScore


class BooleanCategory(Category):
    __tablename__ = "boolean_categories"
    pk = Column(Integer, ForeignKey("categories.pk"), primary_key=True)
    default = Column(Boolean)
    __mapper_args__ = {
        "polymorphic_identity": "BooleanCategory",
    }

    @property
    def score_cls(self):
        return BooleanScore


class NoteCategory(Category):
    """a text field that's not for points, but to tag an sotd association.
    Think of a sticky note of a given colour"""

    __tablename__ = "note_categories"
    pk = Column(Integer, ForeignKey("categories.pk"), primary_key=True)
    property_name = Column(Unicode, nullable=False)
    __mapper_args__ = {
        "polymorphic_identity": "NoteCategory",
    }

    @property
    def score_cls(self):
        return NoteScore

    def set_property(self, value, djudgement):
        sotd_association = djudgement.djudgement_invitation.sotd_association
        sotd_association.get_property(self.property_name).value = bool(value)

    def new_score(self, value, djudgement):
        self.set_property(value, djudgement)

        return self.score_cls(djudgement=djudgement, _value=value, category=self)


class TallyCategory(Category):
    __tablename__ = "tally_categories"
    pk = Column(Integer, ForeignKey("categories.pk"), primary_key=True)
    __mapper_args__ = {
        "polymorphic_identity": "TallyCategory",
    }
    tallies = relationship("SOTDPointsTally", back_populates="category")


class ConditionalValueCategory(TallyCategory):
    """read this as "a score of this category for with value is True is worth
    `weight` points. This *can* also specify an SOTDAssociation property
    name. If so, a score of this category will tag the SOTD in question with
    said property and set its value to the same value. This can be useful for
    concepts such as the partial DQ (e.g., no on theme points) which should
    cause some meta data scores to skip this SOTD when tallying (e.g., the 30
    soapers applies only to on-theme uses of soap)

    """

    __tablename__ = "conditional_val_categories"
    pk = Column(Integer, ForeignKey("tally_categories.pk"), primary_key=True)
    default = Column(Boolean)
    weight = Column(Float, nullable=False)
    property_name = Column(Unicode)
    __mapper_args__ = {
        "polymorphic_identity": "ConditionalValueCategory",
    }

    def __repr__(self):
        return f"{self.title} ({self.weight:.2f} pts{self.djudicial_eligibility_disclaimer})"

    @property
    def score_cls(self):
        return ConditionalValueScore

    def set_property(self, value, djudgement):
        if self.property_name is not None:
            sotd_association = djudgement.djudgement_invitation.sotd_association
            sotd_association.get_property(self.property_name).value = value

    def new_score(self, value, djudgement):
        self.set_property(value, djudgement)

        return self.score_cls(djudgement=djudgement, _value=value, category=self)


class MultipleChoiceCategoryChoice(Base):
    __tablename__ = "multiple_choice_category_choices"
    pk = Column(Integer, primary_key=True)
    category_pk = Column(
        Integer, ForeignKey("multiple_choice_categories.pk"), nullable=False
    )
    label = Column(Unicode, nullable=False)
    weight = Column(Float, nullable=False)
    __table_args__ = (UniqueConstraint("category_pk", "label"),)

    def __repr__(self):
        return f"Choice('{self.label}': {self.weight})"


class MultipleChoiceCategory(TallyCategory):
    __tablename__ = "multiple_choice_categories"
    pk = Column(Integer, ForeignKey("tally_categories.pk"), primary_key=True)
    choices = relationship("MultipleChoiceCategoryChoice")
    choices_by_pk = relationship(
        "MultipleChoiceCategoryChoice",
        collection_class=column_keyed_dict(MultipleChoiceCategoryChoice.pk),
        viewonly=True,
    )
    choices_by_label = relationship(
        "MultipleChoiceCategoryChoice",
        collection_class=column_keyed_dict(MultipleChoiceCategoryChoice.label),
        viewonly=True,
    )
    __mapper_args__ = {"polymorphic_identity": "MultipleChoiceCategory"}

    @property
    def score_cls(self):
        return MultipleChoiceScore

    def new_score(self, value, djudgement):
        return self.score_cls(djudgement=djudgement, _value=value, category=self)


class TextCategory(Category):
    __tablename__ = "text_categories"
    pk = Column(Integer, ForeignKey("categories.pk"), primary_key=True)
    default = Column(UnicodeText)
    __mapper_args__ = {
        "polymorphic_identity": "TextCategory",
    }

    @property
    def score_cls(self):
        return TextScore


class DisqualificationCategory(Category):
    __tablename__ = "dq_categories"
    pk = Column(Integer, ForeignKey("categories.pk"), primary_key=True)
    __mapper_args__ = {
        "polymorphic_identity": "DisqualificationCategory",
    }

    @property
    def score_cls(self):
        return DisqualificationScore


class Score(Base):
    __tablename__ = "scores"
    pk = Column(Integer, primary_key=True)
    djudgement_pk = Column(ForeignKey("djudgements.pk"), nullable=False)
    djudgement = relationship("Djudgement", back_populates="scores")
    category_pk = Column(Integer, ForeignKey("categories.pk"), nullable=False)
    category = relationship("Category", back_populates="scores")
    score_type = Column(Unicode, nullable=False)

    __mapper_args__ = {
        "polymorphic_identity": "Score",
        "polymorphic_on": score_type,
        "with_polymorphic": "*",
    }

    def __repr__(self):
        return f"{self.score_type}({self.value})"

    @property
    def title(self):
        return self.category.title


class NumericScore(Score):
    __tablename__ = "numeric_scores"
    pk = Column(Integer, ForeignKey("scores.pk"), primary_key=True)
    value = Column(Float, nullable=False)
    __mapper_args__ = {
        "polymorphic_identity": "NumericScore",
    }


class BooleanScore(Score):
    __tablename__ = "boolean_scores"
    pk = Column(Integer, ForeignKey("scores.pk"), primary_key=True)
    value = Column(Boolean, nullable=False)
    __mapper_args__ = {
        "polymorphic_identity": "BooleanScore",
    }


class TextScore(Score):
    __tablename__ = "text_scores"
    pk = Column(Integer, ForeignKey("scores.pk"), primary_key=True)
    _value = Column(UnicodeText, nullable=False)
    __mapper_args__ = {
        "polymorphic_identity": "TextScore",
    }

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._set_value(value)

    def _set_value(self, value):
        self._value = value


class NoteScore(TextScore):
    __mapper_args__ = {
        "polymorphic_identity": "NoteScore",
    }

    def has_note(self):
        return bool(self._value)

    @property
    def property_name(self):
        return self.category.property_name

    def _set_value(self, value):
        self._value = value
        self.category.set_property(value, self.djudgement)

    @property
    def note_markdown(self):
        return markdown.markdown(self.value, extensions=[TableExtension()])


class DisqualificationScore(Score):
    __tablename__ = "dq_scores"
    pk = Column(Integer, ForeignKey("scores.pk"), primary_key=True)
    value = Column(UnicodeText, nullable=True)
    __mapper_args__ = {
        "polymorphic_identity": "DisqualificationScore",
    }

    @property
    def disqualified(self):
        return bool(self.value)


class SOTDTallyScore(Score):
    __mapper_args__ = {
        "polymorphic_identity": "SOTDTallyScore",
    }

    @property
    def user_is_eligible(self):
        user_is_djudge = self.djudgement.user in self.category.challenge.active_djudges
        return not user_is_djudge or self.category.djudge_is_eligible


class ConditionalValueScore(SOTDTallyScore):
    """This class represent a numeric value that can only be switched on or off.
    Think of it as a weighted discrete score."""

    __tablename__ = "conditional_value_scores"
    pk = Column(Integer, ForeignKey("scores.pk"), primary_key=True)
    _value = Column(Boolean, nullable=False)

    __mapper_args__ = {
        "polymorphic_identity": "ConditionalValueScore",
    }

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value
        self.category.set_property(value, self.djudgement)

    def __repr__(self):
        return f"{self.score_type}({self.value}::{self.num_val})"

    @property
    def num_val(self):
        """numeric value of this score (i.e., 0. if False, weight if True"""
        return (
            self.category.weight
            if (
                self.value
                and self.user_is_eligible
                and not self.djudgement.disqualified
            )
            else 0.0
        )


class MultipleChoiceScore(SOTDTallyScore):
    __tablename__ = "multiple_choice_scores"
    pk = Column(Integer, ForeignKey("scores.pk"), primary_key=True)
    _value = Column(Integer)
    __mapper_args__ = {
        "polymorphic_identity": "MultipleChoiceScore",
    }

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        if value not in (c.pk for c in self.category.choices):
            raise ValueError(
                f"Value '{value}' not a valid primary key for the choices in "
                f"'{self.category.title}'."
            )
        self._value = value

    @property
    def num_val(self):
        return (
            self.category.choices_by_pk[self.value].weight
            if (
                self.value
                and self.user_is_eligible
                and not self.djudgement.disqualified
            )
            else 0.0
        )
