#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   scavenger_hunt.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   01 Jun 2023

@brief  orm for hashtag scavenger hunts

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from typing import List

from sqlalchemy import ForeignKey, CheckConstraint, select
from sqlalchemy.orm import Mapped, mapped_column, relationship, object_session
from sqlalchemy.sql.expression import false, true

from .. import Base

from .sotd_metadata import (
    WSDBHashTag,
    WSDBRazor,
    WSDBBrush,
    SOTDMetaDataAssociation,
    HashtagMetaDataAssociation,
)
from .sotd import SOTD
from .user import User

from .associations import (
    SOTDAssociation,
    SOTDAssociationProperty,
    SOTDAssociationPropertyAssociation,
)

from typing import Optional

if False:
    from .challenge import ChallengeBase


class TagHuntAssociation(Base):
    __tablename__ = "tag_scavenger_hunt_association"
    wsdb_hashtag_pk: Mapped[int] = mapped_column(
        ForeignKey("wsdb_hashtags.pk"), primary_key=True
    )
    scavenger_hunt_pk: Mapped[int] = mapped_column(
        ForeignKey("scavenger_hunts.pk"), primary_key=True
    )


class ScavengerHunt(Base):
    __tablename__ = "scavenger_hunts"
    pk: Mapped[int] = mapped_column(primary_key=True)
    title: Mapped[str]
    challenge_pk: Mapped[int] = mapped_column(ForeignKey("challenge_bases.pk"))
    challenge: Mapped["ChallengeBase"] = relationship(back_populates="scavenger_hunts")

    metadata_type: Mapped[str] = mapped_column(
        CheckConstraint("metadata_type in ('razor', 'brush')")
    )

    weight: Mapped[float]
    max_score: Mapped[float]

    tags: Mapped[List["WSDBHashTag"]] = relationship(
        secondary="tag_scavenger_hunt_association", order_by="WSDBHashTag.tag"
    )
    # if property_name is set, then this scavenger hunt is only tallied for
    # sotds for which the property of name `property_name` is True
    property_name: Mapped[Optional[str]]

    @property
    def item_type(self):
        if self.metadata_type == "razor":
            return WSDBRazor
        elif self.metadata_type == "brush":
            return WSDBBrush


class ScavengerHuntTally(Base):
    __tablename__ = "scavenger_hunt_tallies"
    pk: Mapped[int] = mapped_column(primary_key=True)
    scavenger_hunt_pk: Mapped[int] = mapped_column(ForeignKey("scavenger_hunts.pk"))
    scavenger_hunt: Mapped[ScavengerHunt] = relationship()

    user_pk: Mapped[int] = mapped_column(ForeignKey("users.pk"))
    user: Mapped["User"] = relationship()

    value: Mapped[float]

    @property
    def approved_tags(self):
        session = object_session(self)
        item_type = self.scavenger_hunt.item_type

        def make_sub_query():
            stmt = (
                select(
                    ScavengerHuntTally.pk.label("tally_pk"),
                    SOTDMetaDataAssociation.pk.label("sotd_metadata_assoc_pk"),
                )
                .select_from(item_type)
                .join(
                    SOTDMetaDataAssociation,
                    SOTDMetaDataAssociation.metadata_item_pk == item_type.pk,
                )
                .where(SOTDMetaDataAssociation.djudicial_dq.is_(False))
                .join(
                    SOTDAssociation,
                    SOTDAssociation.pk == SOTDMetaDataAssociation.sotd_association_pk,
                )
                .join(
                    ScavengerHunt,
                    ScavengerHunt.challenge_pk == SOTDAssociation.challenge_pk,
                )
                .join(SOTD, SOTD.pk == SOTDAssociation.sotd_pk)
                .join(ScavengerHuntTally, ScavengerHuntTally.user_pk == SOTD.user_pk)
                .where(ScavengerHuntTally.pk == self.pk)
                .where(SOTDAssociation.meta_data_checked == true())
                .where(SOTDAssociation.sotd_disqualified == false())
                .where(SOTDAssociation.challenge_pk == self.scavenger_hunt.challenge_pk)
            )

            if self.scavenger_hunt.property_name is not None:
                stmt = (
                    stmt.join(
                        SOTDAssociationPropertyAssociation,
                        SOTDAssociationPropertyAssociation.sotd_association_pk
                        == SOTDAssociation.pk,
                    )
                    .join(
                        SOTDAssociationProperty,
                        SOTDAssociationPropertyAssociation.sotd_association_property_pk
                        == SOTDAssociationProperty.pk,
                    )
                    .where(
                        SOTDAssociationProperty.name
                        == self.scavenger_hunt.property_name
                    )
                    .where(SOTDAssociationPropertyAssociation.value == true())
                )
            return stmt.subquery()

        sub_query = make_sub_query()
        stmt = (
            select(WSDBHashTag)
            .distinct()
            .join(
                HashtagMetaDataAssociation,
                HashtagMetaDataAssociation.hashtag_pk == WSDBHashTag.pk,
            )
            .where(HashtagMetaDataAssociation.djudicial_dq.is_(False))
            .join(
                sub_query,
                sub_query.c.sotd_metadata_assoc_pk
                == HashtagMetaDataAssociation.sotd_metadata_association_pk,
            )
        )
        return session.execute(stmt).scalars().all()

    def update(self):
        self.value = min(
            self.scavenger_hunt.max_score,
            self.scavenger_hunt.weight * len(self.approved_tags),
        )
        return self.value
