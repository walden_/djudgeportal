#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   user.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   02 Jul 2022

@brief  ORM for users and djudges

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from sqlalchemy import (
    Column,
    Integer,
    Boolean,
    Unicode,
    Computed,
)

from typing import List
from sqlalchemy.orm import relationship, Mapped

import secrets
from datetime import datetime, timedelta
from flask_login import UserMixin

from .. import Base


if False:
    from . import SOTDPointsTally, SelectionTally, WSDBHashTag


class User(Base):
    __tablename__ = "users"
    pk = Column(Integer, primary_key=True)
    is_admin = Column(Boolean, nullable=False, default=False)
    user_name = Column(Unicode, nullable=False, unique=True)
    user_name_lower = Column(
        Unicode,
        Computed("lower(user_name)", persisted=True),
        nullable=False,
        unique=True,
    )
    sotds = relationship("SOTD", order_by="SOTD.created_utc", back_populates="author")
    challenges_owned = relationship(
        "ChallengeBase", secondary="owner_association", back_populates="owners"
    )
    challenges_participated = relationship(
        "ChallengeBase",
        secondary="join(SOTDAssociation, SOTD)",
        primaryjoin=("User.pk == SOTD.user_pk"),
        secondaryjoin="ChallengeBase.pk == SOTDAssociation.challenge_pk",
        viewonly=True,
        order_by="ChallengeBase.title",
    )
    challenges_djudged = relationship(
        "ChallengeBase", secondary="djudge_association", back_populates="djudges"
    )

    # for authentication using reddit
    access_code = Column(Integer)
    access_code_valid_until = Column(Integer)
    djudgement_invitations = relationship(
        "DjudgementInvitation",
        secondary="sotd_associations",
        order_by="DjudgementInvitation.issued",
        viewonly=True,
    )
    selections = relationship("Selection", back_populates="djudge")

    open_djudgement_invitations = relationship(
        "DjudgementInvitation",
        secondary="sotd_associations",
        primaryjoin=(
            "and_(SOTDAssociation.djudge_pk == User.pk,"
            "DjudgementInvitation.state != 'Djudged')"
        ),
        order_by="DjudgementInvitation.issued",
        viewonly=True,
    )
    open_selections = relationship(
        "Selection",
        primaryjoin=(
            "and_(Selection.djudge_pk == User.pk, "
            "     Selection.selected_sotd_pk.is_(None))"
        ),
        viewonly=True,
    )

    selected = relationship(
        "Selection",
        primaryjoin=(
            "and_(Selection.djudge_pk == User.pk, "
            "     ~Selection.selected_sotd_pk.is_(None))"
        ),
        viewonly=True,
    )

    @property
    def docket(self):
        return self.open_djudgement_invitations + self.open_selections

    addjudicated = relationship(
        "DjudgementInvitation",
        secondary="sotd_associations",
        primaryjoin=(
            "and_(SOTDAssociation.djudge_pk == User.pk,"
            "DjudgementInvitation.state == 'Djudged')"
        ),
        order_by="DjudgementInvitation.issued",
        viewonly=True,
    )

    sotd_points_tallies: Mapped[List["SOTDPointsTally"]] = relationship(
        back_populates="user"
    )
    selection_tallies: Mapped[List["SelectionTally"]] = relationship(
        back_populates="user"
    )

    tags: Mapped[List["WSDBHashTag"]] = relationship(
        secondary=(
            "join(HashtagMetaDataAssociation, SOTDMetaDataAssociation,"
            "     HashtagMetaDataAssociation.sotd_metadata_association_pk =="
            "     SOTDMetaDataAssociation.pk)"
            ".join(SOTDAssociation, "
            "      SOTDMetaDataAssociation.sotd_association_pk =="
            "      SOTDAssociation.pk)"
            ".join(SOTD, SOTDAssociation.sotd_pk == SOTD.pk)"
        ),
        secondaryjoin="WSDBHashTag.pk == HashtagMetaDataAssociation.hashtag_pk",
        primaryjoin="User.pk == SOTD.user_pk",
        viewonly=True,
    )
    __hash__ = object.__hash__

    @property
    def percentage_djudged(self):
        return 100 * len(self.addjudicated) / (max(1, len(self.djudgement_invitations)))

    @property
    def is_active(self):
        return True

    @property
    def is_authenticated(self):
        return self.is_active

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.pk)

    def __eq__(self, other):
        """
        Checks the equality of two `UserMixin` objects using `get_id`.
        """
        if isinstance(other, UserMixin):
            return self.get_id() == other.get_id()
        return NotImplemented

    def __ne__(self, other):
        """
        Checks the inequality of two `UserMixin` objects using `get_id`.
        """
        equal = self.__eq__(other)
        if equal is NotImplemented:
            return NotImplemented
        return not equal

    # @property
    # def docket(self):
    #     return [dj_inv for dj_inv in self.djudgement_invitations
    #             if dj_inv.state != dj_inv.States.Djudged]

    # @property
    # def addjudicated(self):
    #     return [dj_inv for dj_inv in self.djudgement_invitations
    #             if dj_inv.state == dj_inv.States.Djudged]

    @property
    def sotds_on_docket(self):
        return {inv.sotd for inv in self.docket}

    @property
    def sotd_to_invite(self):
        return {inv.sotd.pk: inv.pk for inv in self.djudgement_invitations}

    @property
    def sotd_to_djudgement(self):
        retval = dict()
        for inv in self.addjudicated:
            print(inv)
            print(inv.djudgement)
            retval[inv.sotd.pk] = inv.djudgement.pk
        print(retval)
        return retval
        # I know above looks dumb, but for some reason, the dict
        # comprehension below crashes jinja
        # return {inv.sotd.pk: inv.djudgement.pk for inv in self.addjudicated}

    # @property
    # def sotds_addjudicated(self):
    #     return {inv.sotd for inv in self.addjudicated}

    def __repr__(self):
        return f"<User('{self.user_name}')>"

    def new_access_code(self, ttl=600, token_size=1000000):
        self.access_code = secrets.randbelow(max(token_size, 1000000))
        self.access_code_valid_until = (
            datetime.now() + timedelta(seconds=ttl)
        ).timestamp()
        return self.access_code

    def send_message(self, subject, message, reddit):
        reddit.redditor(self.user_name).message(subject=subject, message=message)
