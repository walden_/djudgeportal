#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   sotd_metadata.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   17 Apr 2023

@brief  orms to define metadata extracted from sotd posts following the WSDB formatting
        with optional hardware hashtags

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

tts2backend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tts2backend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from .. import Base

from sqlalchemy import Integer, Unicode, Boolean, ForeignKey, UniqueConstraint, func

from typing import List
from sqlalchemy.orm import mapped_column, Mapped, relationship
from collections import namedtuple

from .associations import SOTDAssociation


import re
from ..utils import unmark


class SOTDMetaDataAssociation(Base):
    __tablename__ = "sotd_metadata_association"
    pk: Mapped[int] = mapped_column(primary_key=True)
    sotd_association_pk: Mapped[int] = mapped_column(ForeignKey("sotd_associations.pk"))
    sotd_association: Mapped["SOTDAssociation"] = relationship()
    metadata_item_pk: Mapped[int] = mapped_column(ForeignKey("metadata_items.pk"))
    item: Mapped["MetaDataItem"] = relationship(back_populates="sotds")

    hashtags: Mapped[List["WSDBHashTag"]] = relationship(
        secondary="hashtag_metadata_association",
        secondaryjoin="WSDBHashTag.pk == HashtagMetaDataAssociation.hashtag_pk",
        primaryjoin=(
            "SOTDMetaDataAssociation.pk == "
            "HashtagMetaDataAssociation.sotd_metadata_association_pk"
        ),
        back_populates="sotd_metadata_associations",
    )

    hashtag_associations: Mapped[List["HashtagMetaDataAssociation"]] = relationship(
        order_by="HashtagMetaDataAssociation.hashtag_pk", viewonly=True
    )

    @property
    def challenge(self):
        return self.sotd_association.challenge

    @property
    def sotd(self):
        return self.sotd_association.sotd

    djudicial_dq = mapped_column(Boolean, nullable=False, default=False)

    __table_args__ = (UniqueConstraint(sotd_association_pk, metadata_item_pk),)

    def __repr__(self):
        return f"MetaInfo({self.item}, {self.sotd})"


class HashtagMetaDataAssociation(Base):
    __tablename__ = "hashtag_metadata_association"
    hashtag_pk: Mapped[int] = mapped_column(
        ForeignKey("wsdb_hashtags.pk"), primary_key=True
    )
    hashtag: Mapped["WSDBHashTag"] = relationship(viewonly=True)
    sotd_metadata_association_pk: Mapped[int] = mapped_column(
        ForeignKey("sotd_metadata_association.pk"), primary_key=True
    )
    sotd_metadata_association: Mapped[SOTDMetaDataAssociation] = relationship(
        viewonly=True
    )
    djudicial_dq = mapped_column(Boolean, nullable=False, default=False)


class MetaDataItem(Base):
    __tablename__ = "metadata_items"
    pk = mapped_column(Integer, primary_key=True)
    metadata_type = mapped_column(Unicode, nullable=False)
    hashtag_pattern = re.compile(r"(#[A-Za-z0-9]+)")
    __mapper_args__ = {
        "polymorphic_identity": "wsdb_item",
        "polymorphic_on": metadata_type,
        "with_polymorphic": "*",
    }

    # @property
    # def hashtags(self):
    #     session = object_session(self)
    #     tags = (
    #         session.query(WSDBHashTag)
    #         .join(
    #             HashtagMetaDataAssociation,
    #             HashtagMetaDataAssociation.hashtag_pk == WSDBHashTag.pk,
    #         )
    #         .join(
    #             SOTDMetaDataAssociation,
    #             SOTDMetaDataAssociation.pk
    #             == HashtagMetaDataAssociation.sotd_metadata_association.pk,
    #         )
    #         .filter_by(metadata_item_pk == self.pk)
    #         .all()
    #     )
    #     return tags

    sotds: Mapped[List["SOTDMetaDataAssociation"]] = relationship(back_populates="item")

    @classmethod
    def parse_all(cls, sotd, challenge, session, allowed_hashtag_dict):
        """
        Keyword Arguments:
        sotd    --
        session --
        """
        lines = sotd.text.split("\n")
        sotd_association = (
            session.query(SOTDAssociation)
            .filter_by(sotd=sotd)
            .filter_by(challenge=challenge)
            .one()
        )

        def handle_hashtags(parsed_item, allowed_hashtags, sotd_meta_assoc):
            allowed_hashtags_lower = {ht.lower() for ht in allowed_hashtags}
            for hashtag_str in (
                ht
                for ht in parsed_item.hashtags
                if ht.lower() in allowed_hashtags_lower
            ):
                hashtag = (
                    session.query(WSDBHashTag)
                    .filter(func.lower(WSDBHashTag.tag) == hashtag_str.lower())
                    .one()
                )
                if hashtag not in sotd_meta_assoc.hashtags:
                    sotd_meta_assoc.hashtags.append(hashtag)

        def parse_all_hardware(meta_cl, allowed_hashtags):
            associations = list()
            for line in lines:
                item = meta_cl.parse(line)
                if item is not None:
                    # check whether it exists already
                    metadata_item = (
                        session.query(meta_cl).filter_by(name=item.name).one_or_none()
                    )
                    if metadata_item is None:
                        metadata_item = meta_cl(name=item.name)
                        sotd_meta_assoc = SOTDMetaDataAssociation()
                        sotd_meta_assoc.sotd_association = sotd_association
                        sotd_meta_assoc.item = metadata_item
                        session.add(metadata_item)
                        session.add(sotd_meta_assoc)
                        session.flush()
                    else:
                        sotd_meta_assoc = (
                            session.query(SOTDMetaDataAssociation)
                            .filter_by(sotd_association_pk=sotd_association.pk)
                            .filter_by(metadata_item_pk=metadata_item.pk)
                            .one_or_none()
                        )
                        if sotd_meta_assoc is None:
                            sotd_meta_assoc = SOTDMetaDataAssociation()
                            sotd_meta_assoc.sotd_association = sotd_association
                            sotd_meta_assoc.item = metadata_item
                            session.add(sotd_meta_assoc)
                            session.flush()
                    handle_hashtags(item, allowed_hashtags, sotd_meta_assoc)
                    associations.append(sotd_meta_assoc)
                # only the first item used (1st lather, 1st razor, etc) counts for
                # points, so the remainder is turned to dq by default.
                for association in associations[1:]:
                    association.djudicial_dq = True

            return associations

        def parse_all_software(meta_cl, allowed_hashtags):
            associations = list()
            for line in lines:
                item = meta_cl.parse(line)
                if item is not None:
                    # check whether it exists already
                    metadata_item = (
                        session.query(meta_cl)
                        .filter_by(brand=item.brand)
                        .filter_by(product=item.product)
                        .one_or_none()
                    )
                    if metadata_item is None:
                        metadata_item = meta_cl(brand=item.brand, product=item.product)
                        sotd_meta_assoc = SOTDMetaDataAssociation()
                        sotd_meta_assoc.sotd_association = sotd_association
                        sotd_meta_assoc.item = metadata_item

                        session.add(metadata_item)
                        session.add(sotd_meta_assoc)
                        session.flush()
                    else:
                        sotd_meta_assoc = (
                            session.query(SOTDMetaDataAssociation)
                            .filter_by(sotd_association_pk=sotd_association.pk)
                            .filter_by(metadata_item_pk=metadata_item.pk)
                            .one_or_none()
                        )
                        if sotd_meta_assoc is None:
                            sotd_meta_assoc = SOTDMetaDataAssociation()
                            sotd_meta_assoc.sotd_association = sotd_association
                            sotd_meta_assoc.item = metadata_item
                            session.add(sotd_meta_assoc)
                            session.flush()

                    handle_hashtags(item, allowed_hashtags, sotd_meta_assoc)
                    associations.append(sotd_meta_assoc)
                # only the first item used (1st lather, 1st razor, etc) counts for
                # points, so the remainder is turned to dq by default.
                for association in associations[1:]:
                    association.djudicial_dq = True
            return associations

        for ht_list in allowed_hashtag_dict.values():
            for ht in ht_list:
                if session.query(WSDBHashTag).filter_by(tag=ht).one_or_none() is None:
                    session.add(WSDBHashTag(tag=ht))
                    session.flush()

        metadata = dict()
        metadata["razors"] = parse_all_hardware(
            WSDBRazor,
            allowed_hashtag_dict["razors"]
            if "razors" in allowed_hashtag_dict.keys()
            else [],
        )
        metadata["brushes"] = parse_all_hardware(
            WSDBBrush,
            allowed_hashtag_dict["brushes"]
            if "brushes" in allowed_hashtag_dict.keys()
            else [],
        )
        metadata["lathers"] = parse_all_software(
            WSDBLather,
            allowed_hashtag_dict["lathers"]
            if "lathers" in allowed_hashtag_dict.keys()
            else [],
        )
        metadata["postshaves"] = parse_all_software(
            WSDBPostshave,
            allowed_hashtag_dict["postshaves"]
            if "postshaves" in allowed_hashtag_dict.keys()
            else [],
        )
        metadata["fragrances"] = parse_all_software(
            WSDBFragrance,
            allowed_hashtag_dict["fragrances"]
            if "fragrances" in allowed_hashtag_dict.keys()
            else [],
        )

        return metadata

    @classmethod
    def create_item_parser(cls, item_name):
        """
        parses a string in the form of `* **item_name** ITEM #HASHTAGS`

        Keyword Arguments:
        item_name -- e.g. 'Razor'

        returns a callable that parses lines into a  tuple containing the item and
         a tuple of hashtags
        """
        item_pattern = re.compile(
            r"^" + item_name + r":\s+([^#]*?)(\s*#[a-zA-Z0-9]*)*$",
            re.IGNORECASE,
        )
        parsed = namedtuple("parsed", "name, hashtags")

        def parser(line):
            line = unmark(line.strip())
            match = item_pattern.match(line)
            if match:
                item_name = match.groups()[0]
                hashtags = cls.hashtag_pattern.findall(line)
                return parsed(item_name.strip(), tuple(hashtags))

        return parser


class WSDBHashTag(MetaDataItem):
    __tablename__ = "wsdb_hashtags"
    pk = mapped_column(Integer, ForeignKey("metadata_items.pk"), primary_key=True)
    tag = mapped_column(Unicode, nullable=False, unique=True)

    sotd_metadata_associations: Mapped[List["SOTDMetaDataAssociation"]] = relationship(
        secondary="hashtag_metadata_association",
        secondaryjoin=(
            "SOTDMetaDataAssociation.pk == "
            "HashtagMetaDataAssociation.sotd_metadata_association_pk"
        ),
        primaryjoin="WSDBHashTag.pk == HashtagMetaDataAssociation.hashtag_pk",
        back_populates="hashtags",
        viewonly=True,
    )

    items = relationship(
        "MetaDataItem",
        primaryjoin="WSDBHashTag.pk == HashtagMetaDataAssociation.hashtag_pk",
        secondary="join(SOTDMetaDataAssociation, HashtagMetaDataAssociation)",
        secondaryjoin="MetaDataItem.pk == SOTDMetaDataAssociation.metadata_item_pk",
        viewonly=True,
    )

    __mapper_args__ = {
        "polymorphic_identity": "wsdb_hashtag",
    }

    def __repr__(self):
        return f"Hashtag({self.tag})"


class WSDBHardware(MetaDataItem):
    __mapper_args__ = {
        "polymorphic_identity": "wsdb_hardware",
    }

    @classmethod
    def string_rep(cls, name):
        def __repr__(self):
            return f"{name}({self.name})"

        @property
        def html(self):
            """
            produce html list item
            """
            return f"<strong>{name}:</strong> {self.name}"

        return __repr__, html


class WSDBRazor(WSDBHardware):
    __tablename__ = "wsdb_razors"
    pk = mapped_column(Integer, ForeignKey("metadata_items.pk"), primary_key=True)
    name = mapped_column(Unicode, nullable=False, unique=True)
    __mapper_args__ = {
        "polymorphic_identity": "wsdb_razor",
    }
    category_name = "Razor"

    parse = WSDBHardware.create_item_parser(category_name)
    __repr__, html = WSDBHardware.string_rep(category_name)


class WSDBBrush(WSDBHardware):
    __tablename__ = "wsdb_brushes"
    pk = mapped_column(Integer, ForeignKey("metadata_items.pk"), primary_key=True)
    name = mapped_column(Unicode, nullable=False, unique=True)
    __mapper_args__ = {
        "polymorphic_identity": "wsdb_brush",
    }

    category_name = "Brush"

    parse = WSDBHardware.create_item_parser(category_name)
    __repr__, html = WSDBHardware.string_rep(category_name)


class WSDBSoftware(MetaDataItem):
    __mapper_args__ = {
        "polymorphic_identity": "wsdb_software",
    }

    @classmethod
    def string_rep(cls, name):
        def __repr__(self):
            return f"{name}({self.brand} - {self.product})"

        @property
        def html(self):
            """
            produce html list item
            """
            return f"<strong>{name}:</strong> {self.brand} - {self.product}"

        return __repr__, html

    @classmethod
    def create_software_parser(cls, item_name):
        """
        parses a string in the form of `* **item_name** BRAND - ITEM #HASHTAGS`

        Keyword Arguments:
        item_name -- e.g. 'Razor'

        returns a callable that parses lines into a  tuple containing a tuple of brand
        and product name, and a tuple of hashtags
        """
        base_parser = cls.create_item_parser(item_name)

        parsed = namedtuple("parsed", "brand, product, hashtags")

        def parser(line):
            line = line.strip()
            match = base_parser(line)
            if match:
                item_name, hashtags = match
                brand_match = item_name.replace("–", "-").split("-")[:2]
                if len(brand_match) == 2:
                    return parsed(*(bm.strip() for bm in brand_match), hashtags)

        return parser


class WSDBLather(WSDBSoftware):
    __tablename__ = "wsdb_lathers"
    pk = mapped_column(Integer, ForeignKey("metadata_items.pk"), primary_key=True)
    brand = mapped_column(Unicode, nullable=False)
    product = mapped_column(Unicode, nullable=False)
    __table_args__ = (UniqueConstraint(brand, product),)

    __mapper_args__ = {
        "polymorphic_identity": "wsdb_lather",
    }
    category_name = "Lather"

    parse = WSDBSoftware.create_software_parser(category_name)
    __repr__, html = WSDBSoftware.string_rep(category_name)


class WSDBPostshave(WSDBSoftware):
    __tablename__ = "wsdb_postshaves"
    pk = mapped_column(Integer, ForeignKey("metadata_items.pk"), primary_key=True)
    brand = mapped_column(Unicode, nullable=False)
    product = mapped_column(Unicode, nullable=False)
    __table_args__ = (UniqueConstraint(brand, product),)

    __mapper_args__ = {
        "polymorphic_identity": "wsdb_postshave",
    }

    category_name = "Post Shave"

    parse = WSDBSoftware.create_software_parser(category_name)
    __repr__, html = WSDBSoftware.string_rep(category_name)


class WSDBFragrance(WSDBSoftware):
    __tablename__ = "wsdb_fragrances"
    pk = mapped_column(Integer, ForeignKey("metadata_items.pk"), primary_key=True)
    brand = mapped_column(Unicode, nullable=False)
    product = mapped_column(Unicode, nullable=False)
    __table_args__ = (UniqueConstraint(brand, product),)

    __mapper_args__ = {
        "polymorphic_identity": "wsdb_fragrance",
    }

    category_name = "Fragrance"

    parse = WSDBSoftware.create_software_parser(category_name)
    __repr__, html = WSDBSoftware.string_rep(category_name)
