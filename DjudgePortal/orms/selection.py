#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   selection.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   26 May 2023

@brief  ORM for selections, i.e. legendary post assignment

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from sqlalchemy import ForeignKey, UniqueConstraint, select
from sqlalchemy.orm import Mapped, mapped_column, relationship, object_session
from typing import Optional, List

from .. import Base
from .user import User
from .associations import SOTDAssociation
import markdown
from markdown.extensions.tables import TableExtension

if False:
    from . import SOTD, ChallengeBase


class SelectionSOTDAssociations(Base):
    __tablename__ = "selection_sotd_associations"
    pk: Mapped[int] = mapped_column(primary_key=True)
    selection_pk: Mapped[int] = mapped_column(ForeignKey("selections.pk"))
    sotd_pk: Mapped[int] = mapped_column(ForeignKey("sotds.pk"))


class SelectionTemplate(Base):
    __tablename__ = "selection_templates"
    pk: Mapped[int] = mapped_column(primary_key=True)
    challenge_pk: Mapped[int] = mapped_column(ForeignKey("challenge_bases.pk"))
    challenge: Mapped["ChallengeBase"] = relationship()
    weight: Mapped[float]
    title: Mapped[str]
    guidance: Mapped[str]
    __table_args__ = (UniqueConstraint("challenge_pk", "title"),)

    selections: Mapped[List["Selection"]] = relationship(
        back_populates="selection_template"
    )

    def selected_sotd_associations(self):
        session = object_session(self)
        stmt = (
            select(SOTDAssociation)
            .where(SOTDAssociation.challenge_pk == self.challenge_pk)
            .join(Selection, Selection.selected_sotd_pk == SOTDAssociation.sotd_pk)
            .where(Selection.selection_template_pk == self.pk)
        )
        return list(session.execute(stmt).scalars())

    @property
    def guidance_markdown(self):
        return markdown.markdown(self.guidance, extensions=[TableExtension()])


class Selection(Base):
    __tablename__ = "selections"
    pk: Mapped[int] = mapped_column(primary_key=True)
    djudge_pk: Mapped[int] = mapped_column(ForeignKey(User.pk))
    djudge: Mapped["User"] = relationship(back_populates="selections")
    sotds: Mapped[List["SOTD"]] = relationship(secondary="selection_sotd_associations")
    selection_template_pk: Mapped[int] = mapped_column(ForeignKey(SelectionTemplate.pk))
    selection_template: Mapped[SelectionTemplate] = relationship(
        back_populates="selections"
    )

    selected_sotd_pk: Mapped[Optional[int]] = mapped_column(ForeignKey("sotds.pk"))
    selected_sotd: Mapped[Optional["SOTD"]] = relationship()

    @property
    def weight(self):
        return self.selection_template.weight

    @property
    def is_open(self):
        "Return whether the selection has been performed"
        return self.selected_sotd is None


class SelectionTally(Base):
    __tablename__ = "selection_tallies"
    pk: Mapped[int] = mapped_column(primary_key=True)
    value: Mapped[float]
    user_pk: Mapped[int] = mapped_column(ForeignKey(User.pk))
    user: Mapped[User] = relationship(back_populates="selection_tallies")

    selection_template_pk: Mapped[int] = mapped_column(ForeignKey(SelectionTemplate.pk))
    selection_template: Mapped[SelectionTemplate] = relationship()

    selections: Mapped[List[Selection]] = relationship(
        secondary=(
            "join(Selection, SOTD, SOTD.pk == Selection.selected_sotd_pk)"
            ".join(User, SOTD.user_pk  == User.pk)"
            ".join(SelectionTemplate,"
            "      SelectionTemplate.pk == Selection.selection_template_pk)"
        ),
        primaryjoin=(
            "and_(User.pk == SelectionTally.user_pk,"
            "     SelectionTemplate.pk == SelectionTally.selection_template_pk)"
        ),
        secondaryjoin="SOTD.pk == Selection.selected_sotd_pk",
        viewonly=True,
    )

    def update(self):
        self.value = sum((selection.weight for selection in self.selections))
        return self.value
