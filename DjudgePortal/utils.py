#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   utils.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   17 Apr 2023

@brief  small utility functions

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from markdown import Markdown
from io import StringIO
from html import unescape
from .orms.user import User


def unmark_element(element, stream=None):
    if stream is None:
        stream = StringIO()
    if element.text:
        stream.write(element.text)
    for sub in element:
        unmark_element(sub, stream)
    if element.tail:
        stream.write(element.tail)
    return stream.getvalue()


# patching Markdown
Markdown.output_formats["plain"] = unmark_element
__md = Markdown(output_format="plain")
__md.stripTopLevelTags = False


def unmark(text):
    return unescape(__md.convert(text))


def get_user(db_session, redditor):
    if redditor is None:
        return None
    user = db_session.query(User).filter_by(user_name=redditor.name).one_or_none()
    if not user:
        user = User(user_name=redditor.name)
    return user
