#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   db.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   02 Jul 2022

@brief  Database connection for DjudgePortal

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import random
from os import path

import click
import praw
import prawcore
from flask import current_app, g
from flask.cli import with_appcontext

from sqlalchemy.orm import scoped_session, sessionmaker, close_all_sessions
from sqlalchemy import create_engine
from sqlalchemy import MetaData, func
from sqlalchemy.exc import IntegrityError

from sqlalchemy_schemadisplay import create_schema_graph

from . import Base
from . import orms
from datetime import datetime
import subprocess, shlex, os


def get_db():
    if "db" not in g:
        g.engine = create_engine(current_app.config["SQLALCHEMY_DATABASE_URI"])
        db_session = scoped_session(
            sessionmaker(autocommit=False, autoflush=False, bind=g.engine)
        )
        g.db = db_session
        Base.metadata.create_all(bind=g.engine)

    return g.db


@click.command("init-db")
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo("Initialized the database.")


@click.command("clear-db")
@with_appcontext
def clear_db():
    clear_db_command()


def clear_db_command():
    """Clear the existing data and create new tables."""
    db_session = get_db()
    close_all_sessions()
    Base.metadata.drop_all(bind=db_session.get_bind())
    click.echo("cleared the database.")


@click.command("get-access-token")
@click.argument("user_name")
@with_appcontext
def get_access_token(user_name):
    return get_access_token_command(user_name)


def get_access_token_command(user_name):
    """Generate an access token for user `user_name`"""
    db_session = get_db()
    with db_session.begin():
        query = db_session.query(orms.User).filter_by(user_name=user_name)
        if query.count() != 1:
            click.echo(f"User '{user_name}' does not exist.")
        else:
            user = query.one()
            access_code = user.new_access_code(
                current_app.config["ACCESS_CODE_TTL"],
                current_app.config["ACCESS_CODE_SIZE"],
            )
            click.echo(f"The new access code for {user_name} is '{access_code}'.")


@click.command("add-admin")
@click.argument("user_name")
@with_appcontext
def add_admin(user_name):
    """Generate an access token for user `user_name`"""
    db_session = get_db()
    with db_session.begin():
        query = db_session.query(orms.User).filter_by(user_name=user_name)
        if query.count() == 1:
            click.echo(f"User '{user_name}' already exists!")
        else:
            user = orms.User(user_name=user_name, is_admin=True)
            db_session.add(user)
            click.echo(f"User '{user.user_name}' added.")


@click.command("toggle-admin")
@click.argument("user_name")
@with_appcontext
def toggle_admin(user_name):
    """toggle admin status for user user_name`"""
    db_session = get_db()
    with db_session.begin():
        user = get_user(user_name, db_session)
        user.is_admin = not user.is_admin
        if user.is_admin:
            click.echo(f"User '{user.user_name}' is now admin.")
        else:
            click.echo(f"User '{user.user_name}' is not admin anymore.")
        return 0


@click.command("set-current-post")
@click.argument("group_name")
@with_appcontext
def set_current_post(group_name):
    db_session = get_db()
    with db_session.begin():
        group = (
            db_session.query(orms.ChallengeGroup)
            .filter_by(name=group_name)
            .one_or_none()
        )
        if group is None:
            click.echo(f"Group '{group_name}' does not exist")
            return -1

        reddit = praw.Reddit(
            current_app.config["REDDIT_CONFIG"], user_agent="DjudgePortal"
        )
        url = group.next_post_url
        if not url:
            click.echo("Group '{group_name} does not have the next url set")
        try:
            reddit_post = reddit.submission(url=url)
            reddit_post.comment_sort = "new"
            reddit_post.comments.replace_more(limit=None)
            title = reddit_post.title
        except prawcore.exceptions.PrawcoreException as err:
            err_msg = f"url does not exist on reddit, caught error '{err}'"
            click.echo(err_msg)
            raise Exception(err_msg)
        except praw.exceptions.InvalidURL as e:
            click.echo(e)
            raise Exception(e)
        click.echo(f"found post '{title}'")
        post = (
            db_session.query(orms.Post)
            .filter_by(permalink=reddit_post.permalink)
            .one_or_none()
        )
        if post is None:
            post = orms.Post(permalink=reddit_post.permalink, title=reddit_post.title)
            db_session.add(post)
            db_session.flush()

        post.fill(reddit)
        for challenge in group.challenges:
            challenge.current_post = post
        group.next_post_url = None
        return 0


@click.command("set-next-url-from-title")
@click.argument("title")
@click.argument("group_name")
@click.argument("contact_if_fail")
@with_appcontext
def set_next_url_from_title(title, group_name, contact_if_fail):
    db_session = get_db()
    with db_session.begin():
        group = (
            db_session.query(orms.ChallengeGroup)
            .filter_by(name=group_name)
            .one_or_none()
        )
        if group is None:
            click.echo(f"Group '{group_name}' does not exist")
            return -1

        user = get_user(contact_if_fail, db_session)

        reddit = praw.Reddit(
            current_app.config["REDDIT_CONFIG"], user_agent="DjudgePortal"
        )
        sub = reddit.subreddit("wetshaving")

        def find(title):
            for submission in sub.new(limit=100):
                if title == submission.title:
                    return submission
            raise KeyError(f"title '{title}' not found in the sub")

        try:
            url = find(title).url
            group.next_post_url = url
            click.echo(url)
        except KeyError as err:
            click.echo(err)
            user.send_message(
                "Failed to set next url", f"Error message was '{err}'", reddit
            )
            return -1


@click.command("assign-djudges")
@click.argument("group_name")
@click.argument("template_name")
@click.argument("instructions")
@with_appcontext
def assign_djudges(group_name, template_name, instructions):
    return assign_djudges_cmd(group_name, template_name, instructions)


def assign_djudges_cmd(group_name, template_name, instructions, post_pk=None):
    db_session = get_db()
    with db_session.begin():
        group = (
            db_session.query(orms.ChallengeGroup)
            .filter_by(name=group_name)
            .one_or_none()
        )
        if group is None:
            click.echo(f"Group '{group_name}' does not exist")
            return -1
        challenges = group.challenges
        for challenge in challenges:
            sotds = (
                db_session.query(orms.SOTD)
                .join(
                    orms.SOTDAssociation, orms.SOTD.pk == orms.SOTDAssociation.sotd_pk
                )
                .join(
                    orms.ChallengeBase,
                    orms.SOTDAssociation.challenge_pk == orms.ChallengeBase.pk,
                )
                .filter_by(pk=challenge.pk)
                .outerjoin(
                    orms.DjudgementInvitation,
                    orms.DjudgementInvitation.sotd_association_pk
                    == orms.SOTDAssociation.pk,
                )
            )
            if post_pk is not None:
                sotds = sotds.filter(orms.SOTD.post_pk == post_pk)
            unassigned_sotds = sotds.filter(orms.DjudgementInvitation.pk.is_(None))
            assigned_sotds = sotds.filter(~orms.DjudgementInvitation.pk.is_(None))
            print(f"found {unassigned_sotds.count()} unassigned SOTDs.")
            print(f"found {assigned_sotds.count()} assigned SOTDs.")
            djudges = [
                assoc.djudge for assoc in challenge.djudge_associations if assoc.active
            ]
            print(
                f"found {len(djudges)} djudges, namely {', '.join((dj.user_name for dj in djudges))}."
            )

            djudgepk_counts = (
                db_session.query(
                    orms.SOTDAssociation.djudge_pk,
                    func.count(orms.SOTDAssociation.djudge_pk),
                )
                .group_by(orms.SOTDAssociation.djudge_pk)
                .join(
                    orms.ChallengeBase,
                    orms.SOTDAssociation.challenge_pk == orms.ChallengeBase.pk,
                )
                .filter_by(pk=challenge.pk)
            )

            print("found the following djudge counts:")
            for dj_pk, count in djudgepk_counts:
                if dj_pk:
                    print(f"{db_session.get(orms.User, dj_pk)}: {count}")
            counts_dict = {dj.pk: 1.0 for dj in djudges}
            for pk, count in djudgepk_counts:
                if count:
                    counts_dict[pk] += count

            def weights():
                return [1.0 / counts_dict[dj.pk] ** 3 for dj in djudges]

            template = challenge.djudgement_templates[template_name]
            campaign = orms.DjudgementCampaign(
                template=template, instructions=instructions
            )
            template.djudgement_campaigns.append(campaign)
            for sotd in unassigned_sotds:
                # avoid autodjudgement if multiple djudges exist
                if len(djudges) < 2:
                    use_djudges = djudges
                    use_weights = weights()
                else:
                    use_djudges = list()
                    use_weights = list()
                    for dj, we in zip(djudges, weights()):
                        if dj.user_name_lower != sotd.author.user_name_lower:
                            use_djudges.append(dj)
                            use_weights.append(we)
                djudge = random.choices(use_djudges, use_weights, k=1)[0]
                counts_dict[djudge.pk] += 1
                assignment = campaign.invite_sotd(djudge=djudge, sotd=sotd)
                db_session.add(assignment)
            click.echo(f"challenge '{challenge.name}':")
            db_session.flush()
            for djudge in djudges:
                db_session.refresh(djudge)
                my_docket = assigned_sotds.filter(
                    orms.SOTDAssociation.djudge_pk == djudge.pk
                )
                nb_djudged = my_docket.filter_by(state=orms.States.Djudged)
                nb_unacknowledged = my_docket.filter_by(
                    state=orms.States.Unacknowledged
                )
                click.echo(
                    f"  The Honourable u/{djudge.user_name} was assigned "
                    f"{my_docket.count()} SOTDs of which "
                    f"{nb_djudged.count()} have "
                    f"been djudged and "
                    f"{nb_unacknowledged.count()} are still open."
                )
            click.echo(f"  The challenge has {len(challenge.sotds)} posts.")


@click.command("add-post")
@click.argument("url")
@click.argument("group_name")
@click.argument("day_number")
@with_appcontext
def add_post(url, group_name, day_number):
    return add_post_cmd(url, group_name, day_number)


@click.command("add-AA")
@click.argument("group_name")
@click.argument("day_numbers", type=click.INT, nargs=-1)
@with_appcontext
def add_AA(group_name, day_numbers):
    if not day_numbers:
        return

    def get_title(day):
        year = datetime.now().year
        date = datetime(year, 8, day)
        title = date.strftime("%A Austere August SOTD Thread - %b %d, %Y")
        return title, date

    for day_number in day_numbers:
        try:
            title, date = get_title(day_number)
        except ValueError as err:
            click.echo(err)
            return -1
        reddit = praw.Reddit(
            current_app.config["REDDIT_CONFIG"], user_agent="DjudgePortal"
        )
        sub = reddit.subreddit("Wetshaving")

        post = None
        for submission in sub.new(limit=1000):
            if submission.title == title:
                post = submission
                break
        if post is None:
            click.echo(
                f"Couldn't find AA post for day {day_number} (looking "
                f"for a  post entitled '{title}')"
            )
            return -1

        click.echo(f"Found post '{title}'.")

        add_post_cmd("https://reddit.com" + post.permalink, group_name, day_number)
        assign_djudges_cmd(group_name)


@click.command("add-sotd-to-post")
@click.argument("challenge_name")
@click.argument("day_number")
@click.argument("sotd_url")
@click.argument("user_name")
@with_appcontext
def add_sotd_to_post(challenge_name, day_number, sotd_url, user_name):
    db_session = get_db()
    with db_session.begin():
        challenge = get_challenge(challenge_name, db_session)
        if not challenge.status == orms.ChallengeStatus.Active:
            click.echo(f"Challenge {challenge.name} is not active")
            return -1

        assoc = (
            db_session.query(orms.PostAssociation)
            .filter_by(challenge=challenge, day=day_number)
            .one()
        )
        campaigns = {
            campaign.unambiguously_determine_post_association(): campaign
            for campaign in db_session.query(orms.DjudgementCampaign)
        }
        if assoc not in campaigns.keys():
            click.echo("No campaign associated with this challenge and day found")
            return -1
        campaign = campaigns[assoc]
        click.echo(f"found post {assoc.post.title}.")

        reddit = praw.Reddit(
            current_app.config["REDDIT_CONFIG"], user_agent="DjudgePortal"
        )
        sotd = reddit.comment(url=sotd_url)
        author_name = sotd.author.name if sotd.author else "[deleted]"

        click.echo(f"found sotd by {author_name}")
        if user_name != author_name:
            click.echo(
                f"username provided ('{user_name}') is different from sotd author "
                f"name ('{author_name}')"
            )
            return -1
        if not sotd.parent_id.startswith("t3_"):
            click.confirm(
                "This comment is not a top level comment. Proceed anyway?",
                default=False,
                abort=True,
            )
        if sotd.submission.permalink != assoc.post.permalink:
            message = (
                "This comment has not been posted in the reddit thread that "
                "corresponds to the post found in DjudgePortal. Are you sure you "
                "wish to proceed?\n"
                f"sotd post url: {sotd.submission.permalink}\n"
                f"post post url: {assoc.post.permalink}"
            )
            click.confirm(message, default=False, abort=True)

        user = get_user(sotd.author.name, db_session)
        dj_sotd = (
            db_session.query(orms.SOTD)
            .filter_by(permalink=sotd.permalink)
            .one_or_none()
        )
        if dj_sotd is None:
            dj_sotd = orms.SOTD(
                author=user,
                created_utc=sotd.created_utc,
                post_pk=assoc.post_pk,
                retrieved_on=datetime.now().timestamp(),
                text=sotd.body,
                permalink=sotd.permalink,
            )
            db_session.add(dj_sotd)

            click.confirm(
                f"adding '{dj_sotd}' to {assoc.post.title}?", default=False, abort=True
            )

        if dj_sotd in challenge.sotds:
            click.echo("this SOTD is already part of this challenge")
            return -1
        ret_sotd = challenge.handle_new_comment(
            db_session, assoc.post, dj_sotd, override=True
        )
        db_session.flush()
        db_session.refresh(challenge)
        assert ret_sotd == dj_sotd

        assignment = campaign.invite_sotd(
            sotd=dj_sotd, djudge=random.choices(list(challenge.active_djudges))[0]
        )
        db_session.add(assignment)

    return 0


@click.command("add-campaign-for-current-post")
@click.argument("challenge_name")
@click.argument("day_number")
@click.argument("template_name")
@click.argument("instructions")
@click.option("--additional_filter_term", default=None)
@with_appcontext
def add_campaign_for_current_post(
    challenge_name, day_number, template_name, instructions, additional_filter_term
):
    db_session = get_db()
    with db_session.begin():
        challenge = get_challenge(challenge_name, db_session)
        if not challenge.status == orms.ChallengeStatus.Active:
            click.echo(f"Challenge {challenge.name} is not active")
            return -1
        if challenge.current_post is None:
            click.echo(f"Challenge {challenge.name} does not have a current post set")
            return -2
        assoc = orms.PostAssociation(
            challenge=challenge, post=challenge.current_post, day=day_number
        )
        db_session.add(assoc)
        db_session.flush()
        db_session.refresh(challenge)
        click.echo(
            f"Added post '{challenge.current_post.title}' to challenge "
            f"'{challenge.name}'"
        )
        if additional_filter_term is None:
            additional_filter = lambda x: True
        else:
            additional_filter = lambda text: additional_filter_term in text

        challenge.scan_current_post(additional_filter)

        db_session.flush()
        sotds = (
            db_session.query(orms.SOTD)
            .filter(orms.SOTD.post_pk == challenge.current_post_pk)
            .join(orms.SOTDAssociation, orms.SOTD.pk == orms.SOTDAssociation.sotd_pk)
            .outerjoin(
                orms.DjudgementInvitation,
                orms.DjudgementInvitation.sotd_association_pk
                == orms.SOTDAssociation.pk,
            )
        )

        unassigned_sotds = sotds.filter(orms.DjudgementInvitation.pk.is_(None))
        assigned_sotds = sotds.filter(~orms.DjudgementInvitation.pk.is_(None))
        print(f"found {unassigned_sotds.count()} unassigned SOTDs.")
        print(f"found {assigned_sotds.count()} assigned SOTDs.")
        djudges = [
            assoc.djudge for assoc in challenge.djudge_associations if assoc.active
        ]
        print(
            f"found {len(djudges)} djudges, namely {', '.join((dj.user_name for dj in djudges))}."
        )

        djudgepk_counts = (
            db_session.query(
                orms.SOTDAssociation.djudge_pk,
                func.count(orms.SOTDAssociation.djudge_pk),
            )
            .group_by(orms.SOTDAssociation.djudge_pk)
            .join(
                orms.ChallengeBase,
                orms.SOTDAssociation.challenge_pk == orms.ChallengeBase.pk,
            )
            .filter_by(pk=challenge.pk)
        )

        print("found the following djudge counts:")
        for dj_pk, count in djudgepk_counts:
            if dj_pk:
                print(f"{db_session.get(orms.User, dj_pk)}: {count}")
        counts_dict = {dj.pk: 1.0 for dj in djudges}
        for pk, count in djudgepk_counts:
            if count:
                counts_dict[pk] += count

        def weights():
            return [1.0 / counts_dict[dj.pk] ** 3 for dj in djudges]

        template = challenge.djudgement_templates[template_name]
        campaign = orms.DjudgementCampaign(template=template, instructions=instructions)
        template.djudgement_campaigns.append(campaign)
        for sotd in unassigned_sotds:
            # avoid autodjudgement if multiple djudges exist
            if len(djudges) < 2:
                use_djudges = djudges
                use_weights = weights()
            else:
                use_djudges = list()
                use_weights = list()
                for dj, we in zip(djudges, weights()):
                    if dj.user_name_lower != sotd.author.user_name_lower:
                        use_djudges.append(dj)
                        use_weights.append(we)
            djudge = random.choices(use_djudges, use_weights, k=1)[0]
            counts_dict[djudge.pk] += 1
            assignment = campaign.invite_sotd(djudge=djudge, sotd=sotd)
            db_session.add(assignment)
        click.echo(f"challenge '{challenge.name}':")
        db_session.flush()
        for djudge in djudges:
            db_session.refresh(djudge)
            my_docket = assigned_sotds.filter(
                orms.SOTDAssociation.djudge_pk == djudge.pk
            )
            nb_djudged = my_docket.filter_by(state=orms.States.Djudged)
            nb_unacknowledged = my_docket.filter_by(state=orms.States.Unacknowledged)
            click.echo(
                f"  The Honourable u/{djudge.user_name} was assigned "
                f"{my_docket.count()} SOTDs of which "
                f"{nb_djudged.count()} have "
                f"been djudged and "
                f"{nb_unacknowledged.count()} are still open."
            )
        challenge.current_post = None
        click.echo(f"  The challenge has {len(challenge.sotds)} posts.")


@click.command("add-post-and-campaign")
@click.argument("url")
@click.argument("group_name")
@click.argument("day_number")
@click.argument("template_name")
@click.argument("instructions")
@with_appcontext
def add_post_and_campaign(url, group_name, day_number, template_name, instructions):
    post_pk = add_post_cmd(url, group_name, day_number)
    return assign_djudges_cmd(group_name, template_name, instructions, post_pk)


def add_post_cmd(url, group_name, day_number):
    db_session = get_db()
    with db_session.begin():
        group = (
            db_session.query(orms.ChallengeGroup)
            .filter_by(name=group_name)
            .one_or_none()
        )
        if group is None:
            err_msg = f"Group '{group_name}' does not exist"
            click.echo(err_msg)
            raise KeyError(err_msg)
        reddit = praw.Reddit(
            current_app.config["REDDIT_CONFIG"], user_agent="DjudgePortal"
        )
        try:
            reddit_post = reddit.submission(url=url)
            reddit_post.comment_sort = "new"
            reddit_post.comments.replace_more(limit=None)
            title = reddit_post.title
        except prawcore.exceptions.PrawcoreException as err:
            err_msg = f"url does not exist on reddit, caught error '{err}'"
            click.echo(err_msg)
            raise Exception(err_msg)
        except praw.exceptions.InvalidURL as e:
            click.echo(e)
            raise Exception(e)
        click.echo(f"found post '{title}'")
        post = (
            db_session.query(orms.Post)
            .filter_by(permalink=reddit_post.permalink)
            .one_or_none()
        )
        if post is None:
            post = orms.Post(permalink=reddit_post.permalink, title=reddit_post.title)
            db_session.add(post)
            db_session.flush()
        for challenge in group.challenges:
            if challenge.status == orms.ChallengeStatus.Active:
                assoc = orms.PostAssociation(
                    challenge=challenge, post=post, day=day_number
                )
                db_session.add(assoc)
                db_session.flush()
                db_session.refresh(challenge)
                click.echo(
                    f"Added post '{post.title}' to challenge " f"'{challenge.name}'"
                )
                click.echo("scanning post for sotds")
                post.scan(reddit, db_session)
            else:
                click.echo(
                    f"Can't add post '{post.title}' to challenge '{challenge.name}'"
                    " because it's not active"
                )
        return post.pk


@click.command("add-legendary-selection")
@click.argument("challenge_name")
@click.argument("day_numbers", nargs=-1)
@with_appcontext
def add_legendary_selection(challenge_name, day_numbers):
    db_session = get_db()
    with db_session.begin():
        challenge = get_challenge(challenge_name, db_session)
        template = challenge.selection_templates["Bonus for Legendary Posts"]
        for djudge in challenge.djudges:
            sotds = (
                db_session.query(orms.SOTD)
                .join(orms.SOTDAssociation)
                .filter_by(djudge_pk=djudge.pk)
                .join(orms.Post)
                .join(orms.PostAssociation)
                .filter_by(challenge_pk=challenge.pk)
                .filter(orms.PostAssociation.day.in_(day_numbers))
                .all()
            )
            db_session.add(
                orms.Selection(djudge=djudge, sotds=sotds, selection_template=template)
            )
            click.echo(f"djudge '{djudge.user_name}' selects from {len(sotds)} posts.")
    return 0


def get_challenge(challenge_name, db_session):
    challenge = (
        db_session.query(orms.ChallengeBase)
        .filter_by(name=challenge_name)
        .one_or_none()
    )
    if challenge is None:
        err_msg = f"Challenge '{challenge_name}' does not exist."
        click.echo(err_msg, err=True)
        raise KeyError(err_msg)

    return challenge


@click.command("activate-challenge")
@click.argument("challenge_name")
@with_appcontext
def activate_challenge(challenge_name):
    db_session = get_db()
    with db_session.begin():
        challenge = get_challenge(challenge_name, db_session)
        if challenge.status == orms.ChallengeStatus.Active:
            click.echo(f"Challenge '{challenge_name}' already active.")
            return -2
        challenge.status = orms.ChallengeStatus.Active
        click.echo(f"Activated challenge '{challenge_name}'.")
        return 0


@click.command("add-djudge")
@click.argument("challenge_name")
@click.argument("djudge_names", nargs=-1)
@with_appcontext
def add_djudge(challenge_name, djudge_names):
    db_session = get_db()
    with db_session.begin():
        challenge = get_challenge(challenge_name, db_session)
        for djudge_name in djudge_names:
            djudge = get_user(djudge_name, db_session)
            if djudge in challenge.djudges:
                click.echo(
                    f"Djudge {djudge_name} is already assigned to '{challenge_name}'"
                )
                return -1
            challenge.djudges.append(djudge)
    click.echo(f"added djudges {djudge_names} to '{challenge_name}'")


@click.command("alert-djudges-with-docket")
@click.argument("challenge_group")
@click.option("--preamble", default=None)
@click.option("--subject", default="PSA")
@with_appcontext
def alert_djudges_with_docket(challenge_group, preamble, subject):
    reddit = praw.Reddit(current_app.config["REDDIT_CONFIG"], user_agent="DjudgePortal")
    db_session = get_db()
    with db_session.begin():
        group = (
            db_session.query(orms.ChallengeGroup)
            .filter_by(name=challenge_group)
            .one_or_none()
        )
        if group is None:
            click.echo(f"Group '{challenge_group}' does not exist")
            return -1

        challenge_pks = {ch.pk for ch in group.challenges}

        djudges = (
            db_session.query(orms.User)
            .join(
                orms.DjudgeAssociation, orms.DjudgeAssociation.djudge_pk == orms.User.pk
            )
            .filter(orms.DjudgeAssociation.challenge_pk.in_(challenge_pks))
        )

        djudges = [djudge for djudge in djudges if djudge.docket]

        def create_message(djudge):
            message = (
                f"You have {len(djudge.docket)} "
                f"case{'s' if len(djudge.docket) != 1 else ''} on your docket, "
                "your Honour."
            )
            if preamble:
                message = "\n".join((preamble, message))
            return message

        for djudge in djudges:
            message = "\n".join(
                (
                    f"DjUDGE:  '{djudge.user_name}'",
                    f"SUBJECT: '{subject}'",
                    f"MESSAGE:\n{create_message(djudge)}\n",
                )
            )

            if group.alert_djudges:
                djudge.send_message(
                    subject=subject, message=create_message(djudge), reddit=reddit
                )
            else:
                click.echo(message)


@click.command("serve-access-tokens")
@with_appcontext
def serve_access_tokens():
    reddit = praw.Reddit(current_app.config["REDDIT_CONFIG"], user_agent="DjudgePortal")

    def is_incoming(message):
        return message.dest == reddit.user.me()

    def report(message, reason):
        click.echo(
            f"Couldn't handle message by u/{message.author.name}: "
            f"\nSUBJECT:\n{message.subject}"
            f"\nBODY:\n{message.body}"
            f"\nREASON:\n{reason}"
        )

    def handle_message(message):
        if message is None:
            return
        elif not isinstance(message, praw.models.Message):
            return
        elif not is_incoming(message):
            return
        elif not message.subject.endswith("Access Code"):
            message.reply(
                "I don't understand the command. If you think you did "
                "nothing wrong, please contact a challenge organiser with "
                "a bug report"
            )
            report(message, "unknown command in subject")
            return
        db_session = get_db()
        with db_session.begin():
            user = (
                db_session.query(orms.User)
                .filter_by(user_name=message.author.name)
                .one_or_none()
            )
            if user is None:
                message.reply(
                    "Mhm, according to my records, you are neither an "
                    "organiser of, nor a djudge or participant in any of the"
                    " challenges I'm running. If that sounds wrong to you, "
                    "please contact a challenge organiser with a bug report"
                )
                report(message, f"unknown user u/{message.author.name}")
                return
            message.reply(f"{user.new_access_code()}")
            click.echo(f"served access token for u/{message.author.name}")

    while True:
        try:
            stream = reddit.inbox.stream(pause_after=2, skip_existing=True)
            for message in stream:
                handle_message(message)
        except Exception as err:
            click.echo(err)


@click.command("add-scavenger-hunt")
@click.argument("challenge_name")
@click.argument("scavenger_hunt_title")
@click.argument("metadata_type")
@click.argument("weight")
@click.argument("max_score")
@click.argument("hashtags", nargs=-1)
def add_scavenger_hunt(
    challenge_name, scavenger_hunt_title, metadata_type, weight, max_score, hashtags
):
    db_session = get_db()
    with db_session.begin():
        challenge = (
            db_session.query(orms.ChallengeBase)
            .filter_by(name=challenge_name)
            .one_or_none()
        )
        if challenge is None:
            click.echo(f"Challenge '{challenge_name}' does not exist.")
            return -1
        if challenge.status == orms.ChallengeStatus.Active:
            click.echo(f"Challenge '{challenge_name}' already active.")
            return -2

        def fetch_or_create(name):
            tag = db_session.query(orms.WSDBHashTag).filter_by(tag=name).one_or_none()
            if tag is None:
                tag = orms.WSDBHashTag(tag=name)
                db_session.add(tag)

            return tag

        scavenger_hunt = orms.ScavengerHunt(
            challenge=challenge,
            title=scavenger_hunt_title,
            metadata_type=metadata_type,
            weight=weight,
            max_score=max_score,
        )
        for name in hashtags:
            scavenger_hunt.tags.append(fetch_or_create(name))

        challenge.scavenger_hunts.append(scavenger_hunt)
    return 0


@click.command("add-challenge-group")
@click.argument("group_name")
@click.argument("challenge_names", nargs=-1)
@with_appcontext
def add_challenge_group(group_name, challenge_names):
    if challenge_names:
        group_must_exist = True
    else:
        group_must_exist = False
    db_session = get_db()
    with db_session.begin():
        group = (
            db_session.query(orms.ChallengeGroup)
            .filter_by(name=group_name)
            .one_or_none()
        )
        if group is None and group_must_exist:
            click.echo(
                f"Group '{group_name}' must be created before you can add "
                f"challenges."
            )
            return -1
        if not group:
            group = orms.ChallengeGroup(name=group_name)
            db_session.add(group)
            click.echo(f"added group '{group_name}'")
            return 0
        challenges = list()
        for challenge_name in challenge_names:
            challenge = (
                db_session.query(orms.ChallengeBase)
                .filter_by(name=challenge_name)
                .one_or_none()
            )
            if not challenge:
                click.echo(f"challenge '{challenge_name}' does not exist.")
                return -1
            challenges.append(challenge)
        for challenge in challenges:
            group.challenges.append(challenge)
        click.echo(f"added challenges {challenge_names} to group '{group_name}'")


@click.command("display-schema")
@click.argument("output-path")
def display_schema(output_path):
    db_session = get_db()
    with current_app.app_context():
        # create the pydot graph object by autoloading all tables via a bound metadata
        # object
        metadata = Base.metadata
        metadata.bind = db_session
        metadata.bind.dialect = db_session.get_bind().dialect
        graph = create_schema_graph(
            metadata=metadata,
            show_datatypes=False,  # The image gets nasty big if we'd show the datatypes
            show_indexes=False,  # ditto for indexes
            rankdir="LR",  # From left to right (instead of top to bottom)
            concentrate=False,  # Don't try to join the relation lines together
        )
        graph.write_png(path.join(output_path, "dbschema.png"))


def get_user(user_name, db_session):
    user = (
        db_session.query(orms.User)
        .filter_by(user_name_lower=user_name.lower())
        .one_or_none()
    )
    if user is None:
        reddit = praw.Reddit(
            current_app.config["REDDIT_CONFIG"], user_agent="DjudgePortal"
        )
        praw_user = reddit.redditor(user_name)
        try:  # to make sure the user name exists
            _ = praw_user.id
        except prawcore.exceptions.NotFound as e:
            err_msg = f"User '{user_name}' does not exist on reddit, caught error '{e}'"
            click.echo(err_msg, err=True)
            raise KeyError(err_msg)
        user = orms.User(user_name=praw_user.name)
        click.echo(f"User '{praw_user.name}' found.")
    return user


@click.command("add-keyword-challenge")
@click.argument("owner_name")
@click.argument("challenge_name")
@click.argument("challenge_title")
@click.argument("challenge_url")
@click.argument("challenge_search_string")
@click.argument("challenge_description")
@with_appcontext
def add_keyword_challenge(
    owner_name,
    challenge_name,
    challenge_title,
    challenge_url,
    challenge_search_string,
    challenge_description,
):
    """Generate an access token for user `user_name`"""
    db_session = get_db()
    with db_session.begin():
        owner = get_user(owner_name, db_session)
        try:
            challenge = orms.KeywordChallenge(
                owners=[owner],
                name=challenge_name,
                title=challenge_title,
                post_url=challenge_url,
                search_string=challenge_search_string,
                description=challenge_description,
            )
            db_session.add(challenge)
            db_session.flush()
        except IntegrityError as err:
            click.echo(
                f"couldn't add challenge '{challenge_name}' and caught "
                f"error '{err}' instead"
            )
            return
        click.echo(f"Challenge '{challenge_name}' added.")


@click.command("add-lg-challenge")
@click.argument("owner_name")
@click.argument("challenge_name")
@click.argument("challenge_title")
@click.argument("challenge_url")
@click.argument("challenge_description")
@with_appcontext
def add_lg_challenge(
    owner_name,
    challenge_name,
    challenge_title,
    challenge_url,
    challenge_description,
):
    """Generate an access token for user `user_name`"""
    db_session = get_db()
    with db_session.begin():
        owner = (
            db_session.query(orms.User)
            .filter_by(user_name_lower=owner_name.lower())
            .one_or_none()
        )
        if owner is None:
            reddit = praw.Reddit(
                current_app.config["REDDIT_CONFIG"], user_agent="DjudgePortal"
            )
            praw_user = reddit.redditor(owner_name)
            try:  # to make sure the user name exists
                _ = praw_user.id
            except prawcore.exceptions.NotFound as e:
                click.echo(
                    f"User '{owner_name}' does not exist on reddit, caught error '{e}'"
                )
                return
            owner = orms.User(user_name=praw_user.name)
            click.echo(f"User '{praw_user.name}' found.")

        try:
            challenge = orms.LatherGames2023(
                owners=[owner],
                name=challenge_name,
                title=challenge_title,
                post_url=challenge_url,
                description=challenge_description,
            )
            db_session.add(challenge)
            db_session.flush()
            challenge.create_invite_regular_challenge(db_session)

            db_session.flush()
            challenge.create_invite_special_challenge(db_session)
            db_session.flush()
            challenge.create_invite_extra_special_challenge(db_session)
            challenge.create_legendary_post_template(db_session)

        except IntegrityError as err:
            click.echo(
                f"couldn't add challenge '{challenge_name}' and caught "
                f"error '{err}' instead"
            )
            return

    click.echo(f"Challenge '{challenge_name}' added.")


@click.command("add-fof-challenge")
@click.argument("owner_name")
@click.argument("challenge_name")
@click.argument("challenge_title")
@click.argument("challenge_url")
@click.argument("challenge_description")
@with_appcontext
def add_fof_challenge(
    owner_name,
    challenge_name,
    challenge_title,
    challenge_url,
    challenge_description,
):
    return add_standard_keyword_challenge(
        owner_name,
        challenge_name,
        challenge_title,
        challenge_url,
        challenge_description,
        orms.FeatsOfFragrance2023,
        search_string="#FOF",
    )


@click.command("add-photocontest-challenge")
@click.argument("owner_name")
@click.argument("challenge_name")
@click.argument("challenge_title")
@click.argument("challenge_url")
@click.argument("challenge_description")
@with_appcontext
def add_photocontest_challenge(
    owner_name,
    challenge_name,
    challenge_title,
    challenge_url,
    challenge_description,
):
    return add_standard_keyword_challenge(
        owner_name,
        challenge_name,
        challenge_title,
        challenge_url,
        challenge_description,
        orms.Photocontest2023,
        search_string="#photocontest",
    )


@click.command("add-roty-challenge")
@click.argument("owner_name")
@click.argument("challenge_name")
@click.argument("challenge_title")
@click.argument("challenge_url")
@click.argument("challenge_description")
@with_appcontext
def add_roty_challenge(
    owner_name,
    challenge_name,
    challenge_title,
    challenge_url,
    challenge_description,
):
    return add_standard_keyword_challenge(
        owner_name,
        challenge_name,
        challenge_title,
        challenge_url,
        challenge_description,
        orms.ROTY2023,
        search_string="#ROTY",
    )


@click.command("add-simple-AA-challenge")
@click.argument("owner_name")
@click.argument("challenge_name")
@click.argument("challenge_title")
@click.argument("challenge_url")
@click.argument("challenge_description")
@click.argument("search_string")
@with_appcontext
def add_simple_AA_challenge(
    owner_name,
    challenge_name,
    challenge_title,
    challenge_url,
    challenge_description,
    search_string,
):
    return add_standard_keyword_challenge(
        owner_name,
        challenge_name,
        challenge_title,
        challenge_url,
        challenge_description,
        orms.AustereAugust,
        search_string=search_string,
    )


def add_standard_keyword_challenge(
    owner_name,
    challenge_name,
    challenge_title,
    challenge_url,
    challenge_description,
    challenge_type,
    search_string,
):
    """Generate an access token for user `user_name`"""
    db_session = get_db()
    with db_session.begin():
        owner = (
            db_session.query(orms.User)
            .filter_by(user_name_lower=owner_name.lower())
            .one_or_none()
        )
        if owner is None:
            reddit = praw.Reddit(
                current_app.config["REDDIT_CONFIG"], user_agent="DjudgePortal"
            )
            praw_user = reddit.redditor(owner_name)
            try:  # to make sure the user name exists
                _ = praw_user.id
            except prawcore.exceptions.NotFound as e:
                click.echo(
                    f"User '{owner_name}' does not exist on reddit, caught error '{e}'"
                )
                return
            owner = orms.User(user_name=praw_user.name)
            click.echo(f"User '{praw_user.name}' found.")

        try:
            challenge = challenge_type(
                owners=[owner],
                name=challenge_name,
                title=challenge_title,
                post_url=challenge_url,
                description=challenge_description,
                search_string=search_string,
            )
            db_session.add(challenge)
            db_session.flush()
            challenge.create_invite(db_session)

        except IntegrityError as err:
            click.echo(
                f"couldn't add challenge '{challenge_name}' and caught "
                f"error '{err}' instead"
            )
            return

    click.echo(f"Challenge '{challenge_name}' added.")


@click.command("add-sponsor_points")
@click.argument("challenge_name")
@click.argument("sponsor_points_title")
@click.argument("sponsor_type")
@click.argument("weight")
@click.argument("max_score")
@click.argument("bonus")
@click.option("--property_name", default=None)
@click.argument("sponsor_names", nargs=-1)
@with_appcontext
def add_sponsor_points(
    challenge_name,
    sponsor_points_title,
    sponsor_type,
    weight,
    max_score,
    bonus,
    property_name,
    sponsor_names,
):
    db_session = get_db()
    with db_session.begin():
        challenge = get_challenge(challenge_name, db_session)

        def fetch_or_create_sponsor(name):
            sponsor = db_session.query(orms.Sponsor).filter_by(name=name).one_or_none()
            if sponsor is None:
                sponsor = orms.Sponsor(name=name)
            return sponsor

        sponsor_points = orms.SponsorPoints(
            challenge=challenge,
            title=sponsor_points_title,
            weight=weight,
            max_score=max_score,
            bonus_for_reaching_max=bonus,
            sponsor_type=sponsor_type,
            sponsors=[fetch_or_create_sponsor(name) for name in sponsor_names],
            property_name=property_name,
        )
        challenge.sponsor_points.append(sponsor_points)
        click.echo(f"Added sponsor points '{sponsor_points.title}'")
        return 0


@click.command("add-bonus-points")
@click.argument("challenge_name")
@click.argument("bonus_title")
@click.argument("threshold")
@click.argument("bonus")
@click.argument("bonus_type")
@with_appcontext
def add_bonus_points(challenge_name, bonus_title, threshold, bonus, bonus_type):
    db_session = get_db()
    with db_session.begin():
        challenge = get_challenge(challenge_name, db_session)

        new_bonus = orms.UsageBonus(
            title=bonus_title,
            challenge=challenge,
            threshold=threshold,
            bonus=bonus,
            usage_bonus_type=bonus_type,
        )
        db_session.add(new_bonus)

    click.echo(f"Added bonus points '{new_bonus.title}' to '{challenge_name}'")
    return 0


@click.command("dump-db")
@with_appcontext
def dump_db():
    return dump_db_command(current_app.config["SQLALCHEMY_DATABASE_URI"])


def dump_db_command(uri):
    command = shlex.split(f"pg_dump {uri}")
    subprocess.run(command, check=True)


@click.command("safely-load-db")
@click.argument("file_name")
@click.option("--directory", default=None)
@with_appcontext
def safely_load_db(file_name, directory):
    uri = current_app.config["SQLALCHEMY_DATABASE_URI"]
    backup_fname = f"db_dump_backup_{datetime.now().isoformat(timespec='seconds')}.sql"
    if directory is not None:
        backup_fname = os.path.join(directory, backup_fname)
    with open(backup_fname, "w") as fh:
        proc = subprocess.run(shlex.split(f"pg_dump {uri}"), stdout=fh, check=True)
    if proc.returncode != 0:
        click.echo(f"db dump returned error code {proc.returncode}")
        return proc.returncode
    click.echo(f"written db backup to '{backup_fname}'")

    clear_db_command()

    with open(file_name, "r") as fh:
        proc = subprocess.run(shlex.split(f"psql {uri}"), stdin=fh, capture_output=True)
    if proc.returncode != 0:
        click.echo(f"db load returned error code {proc.returncode}")
        return proc.returncode
    click.echo(f"loaded db from file '{file_name}'")


@click.command("retally-for-user")
@click.argument("challenge_name")
@click.argument("user_name")
@with_appcontext
def retally_for_user(challenge_name, user_name):
    db_session = get_db()
    with db_session.begin():
        challenge = get_challenge(challenge_name, db_session)
        if not challenge.status == orms.ChallengeStatus.Active:
            click.echo(f"Challenge {challenge.name} is not active")
            return -1
        user = get_user(user_name, db_session)

        challenge.update_tally_for_user(user, db_session)
        return 0


@click.command("retally-challenge")
@click.argument("challenge_name")
@with_appcontext
def retally_challenge(challenge_name):
    db_session = get_db()
    with db_session.begin():
        challenge = get_challenge(challenge_name, db_session)
        if not challenge.status == orms.ChallengeStatus.Active:
            click.echo(f"Challenge {challenge.name} is not active")
            return -1

        with click.progressbar(challenge.participants) as users:
            for user in users:
                challenge.update_tally_for_user(user, db_session)
        return 0


def close_db(e=None):
    db = g.pop("db", None)

    if db is not None:
        db.remove()


def init_db():
    db_session = get_db()

    try:
        with current_app.app_context():
            for tbl in reversed(MetaData().sorted_tables):
                db_session.session.get_bind().execute(tbl.delete())
            Base.metadata.create_all(bind=db_session.get_bind())
    except Exception as err:
        raise Exception(
            f"caught error '{err}' while trying to create connection "
            f"'{current_app.config['SQLALCHEMY_DATABASE_URI']}"
        )


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(activate_challenge)
    app.cli.add_command(add_AA)
    app.cli.add_command(add_admin)
    app.cli.add_command(add_lg_challenge)
    app.cli.add_command(add_fof_challenge)
    app.cli.add_command(add_photocontest_challenge)
    app.cli.add_command(add_roty_challenge)
    app.cli.add_command(add_simple_AA_challenge)
    app.cli.add_command(add_challenge_group)
    app.cli.add_command(add_djudge)
    app.cli.add_command(add_keyword_challenge)
    app.cli.add_command(add_post)
    app.cli.add_command(add_post_and_campaign)
    app.cli.add_command(add_scavenger_hunt)
    app.cli.add_command(assign_djudges)
    app.cli.add_command(clear_db)
    app.cli.add_command(display_schema)
    app.cli.add_command(get_access_token)
    app.cli.add_command(init_db_command)
    app.cli.add_command(serve_access_tokens)
    app.cli.add_command(add_legendary_selection)
    app.cli.add_command(add_sponsor_points)
    app.cli.add_command(add_bonus_points)
    app.cli.add_command(set_current_post)
    app.cli.add_command(add_campaign_for_current_post)

    app.cli.add_command(dump_db)
    app.cli.add_command(safely_load_db)

    app.cli.add_command(alert_djudges_with_docket)

    app.cli.add_command(set_next_url_from_title)
    app.cli.add_command(toggle_admin)
    app.cli.add_command(retally_for_user)
    app.cli.add_command(retally_challenge)
    app.cli.add_command(add_sotd_to_post)
    # db.init_app(app)
