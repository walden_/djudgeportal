#!/usr/bin/env bash
# @file   format.sh
#
# @author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>
#
# @date   14 Apr 2023
#
# @brief  lints the source files and checks for common errors
#
# Copyright © 2023 Dj Djundjila, TTS Rebuild Committee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -euxo pipefail 

export files="setup.py tests/*.py DjudgePortal/*.py DjudgePortal/*/*.py"

black --required-version 23 --check $files

pyflakes $files

