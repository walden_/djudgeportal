#!/usr/bin/env bash
# @file   setup_lg_test_challenge.sh
#
# @author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>
#
# @date   10 May 2023
#
# @brief  sets up a test lg challenge
#
# Copyright © 2023 Dj Djundjila, TTS Rebuild Committee
#
# DjudgePortal is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DjudgePortal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
set -Eeuo pipefail

export CHALLENGE_NAME=test_lg

flask --app DjudgePortal clear-db
flask --app DjudgePortal init-db
flask --app DjudgePortal add-admin admin
flask --app DjudgePortal get-access-token admin
flask --app DjudgePortal add-lg-challenge djundjila ${CHALLENGE_NAME} ${CHALLENGE_NAME}_title https://www.reddit.com/r/Wetshaving/comments/ucik9d/announcing_the_eighth_annual_lather_games/ "Test instance using lg 22 posts"
flask --app DjudgePortal add-djudge ${CHALLENGE_NAME} djundjila merikus uss-spongebob _walden_ wallygator88 DoctorRotor J33pGuy13 VisceralWatch semaj3000 RedmosquitoMM jeffm54321 fuckchalzone scorpio93x old_hiker hairykopite mrtangerinesky

flask --app DjudgePortal add-scavenger-hunt ${CHALLENGE_NAME} "Razor #Hunt" razor 0.1 1. \#COMB \#TWIST \#SLANT \#GEM \#INJECTOR \#ARTISTCLUB \#CUTTHROAT \#JAPANESE \#PENCILTHIN \#OLDTIMER \#BUTCHERKNIFE \#ALUMINUM \#BRASS \#PLASTIC \#STAINLESS \#TITANIUM \#CHROME \#GOLD \#NICKEL \#MACHINEAGE \#ATOMICAGE \#SPACEAGE


flask --app DjudgePortal add-scavenger-hunt ${CHALLENGE_NAME} "Brush #Hunt" brush 0.1 1. \#MACHINEAGE \#ATOMICAGE \#SPACEAGE \#UNICOLOR \#BICOLOR \#TRICOLOR \#TANGLED \#WHOLEHOG \#FAUXBADGER \#SILVERTIP  \#TWOBAND \#PURE \#IMPURE \#DOORKNOB \#WOODEN \#SMOLL \#CHONK \#STUBBY \#BARBER \#FAN \#SHD

flask --app DjudgePortal add-sponsor_points --property_name="on theme" ${CHALLENGE_NAME} "Software Sponsor Points" software 1. 13. 1. "Barrister and Mann" "Catie's Bubbles" "Chicago Grooming Co. / Oleo Soapworks" "House of Mammoth" "Maggard Razors (house-brand soap)" "Noble Otter" "Shawn Maher (Chatillon Lux, Maher Olfactive)" "Southern Witchcrafts" "Spearhead Shaving" "Stirling Soap Co." "Summer Break Soaps" "Wholly Kaw" "Zingari Man"

flask --app DjudgePortal add-sponsor_points ${CHALLENGE_NAME} "Hardware Sponsor Points" hardware 1. 2. 1. "AP Shave Co complete brushes or Handcrafted Series handles" "Blackland razors" "Chisel and Hound brushes" "Dogwood Handcrafts brushes" "Maggard Razors vintage/restored razors" "Summer Break Soaps brushes" "Wolfman Razors brushes or razors" "Yates razors"

export NB_FOR_BONUS=3 # CHANGE THIS FOR LG
export VAL_BONUS=1.0
flask --app DjudgePortal add-bonus-points ${CHALLENGE_NAME} "30 Lather Brands Bonus" ${NB_FOR_BONUS} ${VAL_BONUS} LatherBrandBonus
flask --app DjudgePortal add-bonus-points ${CHALLENGE_NAME} "30 Lather Scents Bonus" ${NB_FOR_BONUS} ${VAL_BONUS} LatherScentBonus
flask --app DjudgePortal add-bonus-points ${CHALLENGE_NAME} "30 Post Shaves Bonus" ${NB_FOR_BONUS} ${VAL_BONUS} PostshaveBonus
flask --app DjudgePortal add-bonus-points ${CHALLENGE_NAME} "30 Frangrance Bonus" ${NB_FOR_BONUS} ${VAL_BONUS} FragranceBonus

flask --app DjudgePortal activate-challenge ${CHALLENGE_NAME}

flask --app DjudgePortal add-challenge-group main_group
flask --app DjudgePortal add-challenge-group main_group ${CHALLENGE_NAME}
flask --app DjudgePortal add-post-and-campaign https://reddit.com/r/Wetshaving/comments/v2a1la/wednesday_lather_games_sotd_thread_jun_01_2022/ main_group 1 "DailyChallenge" "Theme is topical and challenge is challenging"

flask --app DjudgePortal add-post-and-campaign https://reddit.com/r/Wetshaving/comments/v32374/thursday_lather_games_sotd_thread_jun_02_2022/ main_group 2 "Special Challenge (1/2)" "Theme is topical and challenge is special"

flask --app DjudgePortal add-post-and-campaign https://reddit.com/r/Wetshaving/comments/v3sr4i/friday_lather_games_sotd_thread_jun_03_2022/ main_group 3 "Special Challenge (1/1)" "Theme is topical and challenge is extra challenging"

flask --app DjudgePortal add-legendary-selection ${CHALLENGE_NAME} 1 2
flask --app DjudgePortal add-legendary-selection ${CHALLENGE_NAME} 3



