#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_sprint_scores.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   27 May 2023

@brief  testing the Selection mechanism

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from flask import g
from DjudgePortal import orms
from DjudgePortal.db import get_db


def test_selection(lg_challenge_with_many_sotd_post_2_lg):
    db_session = get_db()
    weight = 2.5
    with db_session.begin():
        djudge1 = db_session.get(orms.User, g.djudge1_pk)
        djudge2 = db_session.get(orms.User, g.djudge2_pk)

        lg = db_session.get(orms.LatherGames2023, g.lg_pks[0])

        template = orms.SelectionTemplate(
            challenge=lg, weight=weight, title="legendary post", guidance="which one?"
        )
        db_session.add(template)

    with db_session.begin():
        sotds = lg.sotds

        selection1 = orms.Selection(
            djudge=djudge1,
            sotds=sotds[::2],
            selection_template=template,
            selected_sotd=sotds[0],
        )

        selection2 = orms.Selection(
            djudge=djudge2,
            sotds=sotds[1::2],
            selection_template=template,
            selected_sotd=sotds[1],
        )

        db_session.add_all((selection1, selection2))

    with db_session.begin():
        lg = db_session.get(orms.LatherGames2023, g.lg_pks[0])
        for participant in lg.participants:
            lg.update_tally_for_user(participant, db_session)
        assert len(lg.selection_tallies) == 2
        for tally in lg.selection_tallies:
            assert tally.value == weight
    with db_session.begin():
        lg = db_session.get(orms.LatherGames2023, g.lg_pks[1])
        for participant in lg.participants:
            lg.update_tally_for_user(participant, db_session)
        assert len(lg.selection_tallies) == 0
