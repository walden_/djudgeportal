#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_score.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   03 May 2023

@brief  test scores and categories

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from DjudgePortal.db import get_db
from DjudgePortal import orms
from flask import g
import pytest


@pytest.mark.parametrize(
    "category_type, default, score_type",
    [
        (orms.NumericCategory, 1.2, orms.NumericScore),
        (orms.BooleanCategory, 1.2, orms.BooleanScore),
        (orms.TextCategory, 1.2, orms.TextScore),
    ],
)
def test_category_defaults(
    app_with_keyword_challenges, category_type, default, score_type
):
    db_session = get_db()
    with db_session.begin():
        category = category_type(title="title", guidance="guidance", default=default)
        assert category.default == default
        assert category.score_cls == score_type


def test_repr(lg_challenge_with_incomplete_djudgement):
    db_session = get_db()
    with db_session.begin():
        djudgement = db_session.get(orms.Djudgement, g.djudgement_pk)
        lg = db_session.get(orms.ChallengeBase, g.lg_pk)

        djudgement.scores.append(
            lg.categories["Daily Challenge"].new_score(
                value=True, djudgement=djudgement
            )
        )
        assert djudgement.scores[-1].__repr__() == "ConditionalValueScore(True::0.2)"

        djudgement.scores.append(
            lg.categories["Disqualification"].new_score(
                value="Disqualification",
                djudgement=djudgement,
            )
        )
        assert (
            djudgement.scores[-1].__repr__()
            == "DisqualificationScore(Disqualification)"
        )
        assert djudgement.scores[-1].title == "Disqualification"
