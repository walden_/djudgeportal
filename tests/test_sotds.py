#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_sotds.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   13 Apr 2023

@brief  description

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

tts2backend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tts2backend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from DjudgePortal.db import get_db
from DjudgePortal import orms

from datetime import datetime
from unittest.mock import MagicMock, Mock


def test_view_sotds_for_user_in_challenge_admin(client_with_keyword_challenges, auth):
    response = client_with_keyword_challenges.get("/sotd/1/101")
    assert b"submitted 0 SOTDS." in response.data


def test_view_sotds_for_user_in_challenge_author(client_with_keyword_challenges, auth):
    db_session = get_db()
    with db_session.begin():
        sotd = db_session.get(orms.SOTD, 1)
        challenge_pk = sotd.challenges[0].pk
        assert len(sotd.challenges) == 2
        author_pk = sotd.author.pk
    response = client_with_keyword_challenges.get(f"/sotd/{author_pk}/{challenge_pk}")
    assert b"submitted 2 SOTDS." in response.data


def test_scan_post(client_with_keyword_challenges):
    db_session = get_db()
    with db_session.begin():
        challenge = db_session.get(orms.KeywordChallenge, 101)
        nb_sotds_before = len(challenge.sotds)
        mock_reddit = Mock()
        mock_author = Mock()
        mock_author.name = "author"
        mock_comment = Mock()
        mock_comment.body = challenge.search_string
        mock_comment.created_utc = datetime.now().timestamp()
        mock_comment.author = mock_author
        mock_comment.permalink = "r/sub/3youst"
        mock_post = Mock()
        mock_post.comments = MagicMock()
        mock_post.comments.__iter__.return_value = [mock_comment]
        mock_reddit.submission = MagicMock(return_value=mock_post)

        post = db_session.get(orms.Post, 1)
        post.scan(mock_reddit, db_session)
        assert len(challenge.sotds) == 1 + nb_sotds_before
        assert challenge.sotds[-1].text == challenge.search_string
        assert challenge.sotds[-1].author.user_name == "author"
