#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_challenges.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   30 Jul 2022

@brief  test for the challenges overview blue print

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging


def test_index(client_with_keyword_challenges, auth_with_keyword_challenges):
    response = client_with_keyword_challenges.get("/")
    assert b"Log In" in response.data
    # assert b"Register" in response.data

    auth_with_keyword_challenges.login()
    response = client_with_keyword_challenges.get("/")
    logging.debug(response.data.decode())
    print(response.data.decode())
    assert b"Log Out" in response.data
    assert b"challenge 1" in response.data
    assert b"challenge 2" in response.data
    assert b"by" in response.data
    assert b"logged in as user" in response.data
    auth_with_keyword_challenges.logout()
    response = client_with_keyword_challenges.get("/")
    assert b"logged in" not in response.data
