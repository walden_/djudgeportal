#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_factory.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   19 Jul 2022

@brief  tests the app factory

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from DjudgePortal import create_app


def test_config():
    assert not create_app({"SQLALCHEMY_DATABASE_URI": "sqlite:///:memory:"}).testing
    assert create_app(
        {"TESTING": True, "SQLALCHEMY_DATABASE_URI": "sqlite:///:memory:"}
    ).testing


def test_index(client):
    response = client.get("/index")
    assert response.data.decode().split("\n")[1] == "<!doctype html>"
