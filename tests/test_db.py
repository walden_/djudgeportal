#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_db.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   29 Jul 2022

@brief  tests for the db module

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from DjudgePortal.db import get_db


def test_get_close_db(empty_app):
    with empty_app.app_context():
        db_session = get_db()
        assert db_session is get_db()


def test_init_db_command(runner, monkeypatch):
    class Recorder(object):
        called = False

    def fake_init_db():
        Recorder.called = True

    monkeypatch.setattr("DjudgePortal.db.init_db", fake_init_db)
    result = runner.invoke(args=["init-db"])
    assert "Initialized" in result.output
    assert Recorder.called
