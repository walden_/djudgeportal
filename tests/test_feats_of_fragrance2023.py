#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_feats_of_fragrance2023.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   22 Jun 2023

@brief  test the feats of fragrance setup

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import pytest
from flask import g
from DjudgePortal import orms
from DjudgePortal.db import get_db


@pytest.mark.parametrize(
    ("label", "score"),
    [
        ("0", 0.0),
        ("1", 1.0),
        ("2", 2.0),
        ("3", 3.0),
        ("4", 4.0),
        ("5", 5.0),
    ],
)
def test_djudgement_value(app_with_fof_challenge, label, score):
    db_session = get_db()
    with db_session.begin():
        djudge = db_session.get(orms.User, g.djudge1_pk)
        fof = db_session.get(orms.ChallengeBase, g.fof_pk)
        sotd = db_session.get(orms.SOTD, g.sotd_pk)

        template = fof.create_invite(db_session=db_session)
        campaign = orms.DjudgementCampaign(
            template=template,
            instructions=("FOF for day 1"),
        )

        db_session.add_all((campaign, template))

    with db_session.begin():
        invitation = campaign.invite_sotd(djudge=djudge, sotd=sotd)
        db_session.add(invitation)

    with db_session.begin():
        # djudge sotd
        djudgement = orms.Djudgement(djudgement_invitation=invitation)
        db_session.add(djudgement)
        djudgement.scores.append(
            fof.categories["Score"].new_score(
                value=fof.categories["Score"].choices_by_label[label].pk,
                djudgement=djudgement,
            )
        )
    with db_session.begin():
        fof.update_tally_for_user(sotd.author, db_session)

    with db_session.begin():
        for tally in sotd.author.sotd_points_tallies:
            assert tally.value == score
