#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_scavenger_hunts.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   02 Jun 2023

@brief  testing scavenger hunts

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import pytest
from flask import g
from DjudgePortal import orms
from DjudgePortal.db import get_db

from sqlalchemy.exc import IntegrityError


def test_wrong_metadata_type(app_with_lg_challenge):
    db_session = get_db()
    with db_session.begin():
        lg = db_session.get(orms.ChallengeBase, g.lg_pk)
        lg.scavenger_hunts.append(
            orms.ScavengerHunt(
                challenge=lg,
                title="Wrong #Hunt",
                metadata_type="WRONG_TYPE",
                weight=12.8,
                max_score=24.0,
            )
        )

        with pytest.raises(IntegrityError):
            db_session.flush()
