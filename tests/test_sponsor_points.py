#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_sponsor_points.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   03 Jun 2023

@brief  tests for sponsor point tallying

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import g
from DjudgePortal import orms
from DjudgePortal.db import get_db


def test_sponsor_tallying(lg_challenge_with_2_sotd_2_lg):
    db_session = get_db()
    weight = 1.2
    max_score = 3.0
    bonus = 0.5

    with db_session.begin():
        challenge = db_session.get(orms.ChallengeBase, g.lg_pks[0])

        author1, author2 = (
            db_session.get(orms.User, pk) for pk in list(g.author_ids)[:2]
        )
        sotd1 = [sotd for sotd in challenge.sotds if sotd.author == author1][:1][0]
        sotd1_assoc = (
            db_session.query(orms.SOTDAssociation)
            .filter_by(sotd_pk=sotd1.pk)
            .filter_by(challenge_pk=challenge.pk)
            .one()
        )
        sotd2 = [sotd for sotd in challenge.sotds if sotd.author == author2][:1][0]
        sotd2_assoc = (
            db_session.query(orms.SOTDAssociation)
            .filter_by(sotd_pk=sotd2.pk)
            .filter_by(challenge_pk=challenge.pk)
            .one()
        )
        noble_otter = orms.Sponsor(name="Noble Otter")
        stirling = orms.Sponsor(name="Stirling Soap Company")
        chatillon_lux = orms.Sponsor(name="Chatillon Lux")
        sponsors = [noble_otter, stirling, chatillon_lux]
        software_sponsor_points = orms.SponsorPoints(
            challenge=challenge,
            title="Software Sponsor Points",
            weight=weight,
            max_score=max_score,
            bonus_for_reaching_max=bonus,
            sponsor_type="software",
            sponsors=sponsors,
        )
        challenge.sponsor_points.append(software_sponsor_points)

    with db_session.begin():
        db_session.add(
            orms.SponsorUse(
                sponsor_points=software_sponsor_points,
                sponsor=noble_otter,
                sotd_association=sotd1_assoc,
            )
        )

        tally1, tally2 = (
            orms.SponsorPointsTally(sponsor_points=software_sponsor_points, user=author)
            for author in (author1, author2)
        )
        db_session.add_all((tally1, tally2))
        db_session.flush()
        assert tally1.update() == 0.0
        assert tally2.update() == 0.0

        for association in db_session.query(orms.SOTDAssociation):
            association.meta_data_checked = True
            association.get_property("on theme").value = True

    with db_session.begin():
        assert tally1.update() == weight
        assert tally2.update() == 0.0

        db_session.add(
            orms.SponsorUse(
                sponsor_points=software_sponsor_points,
                sponsor=noble_otter,
                sotd_association=sotd2_assoc,
            )
        )

    with db_session.begin():
        assert tally1.update() == weight
        assert tally2.update() == weight

        db_session.add(
            orms.SponsorUse(
                sponsor_points=software_sponsor_points,
                sponsor=stirling,
                sotd_association=sotd2_assoc,
            )
        )

    with db_session.begin():
        assert tally1.update() == weight
        assert tally2.update() == 2 * weight

        # yes, double use of the same sponsor
        db_session.add(
            orms.SponsorUse(
                sponsor_points=software_sponsor_points,
                sponsor=stirling,
                sotd_association=sotd2_assoc,
            )
        )

    with db_session.begin():
        assert tally1.update() == weight
        assert tally2.update() == 2 * weight

        db_session.add(
            orms.SponsorUse(
                sponsor_points=software_sponsor_points,
                sponsor=chatillon_lux,
                sotd_association=sotd2_assoc,
            )
        )

    with db_session.begin():
        assert tally1.update() == weight
        assert tally2.update() == max_score + bonus
